enablePlugins(Antlr4Plugin)

lazy val genSrc = taskKey[List[File]]("generate sources")

genSrc := (sourceGenerators in Compile) { _.join.map(_.flatten.toList) }.value

lazy val makeClasspathFile = taskKey[Unit]("write the run-time classpath to target/jars.txt as colon-separated list")

lazy val root = (project in file(".")).
  settings(
    organization := "org.microvm",

    name := "mu-impl-ref2",

    version := "2.2.0",

    description := "The second reference implementation of Mu, the micro virtual machine",

    licenses := Seq("CC BY-SA 4.0" -> url("https://creativecommons.org/licenses/by-sa/4.0/legalcode")),

    scalaVersion := "2.12.6",

    libraryDependencies ++= Seq(
        "org.antlr" % "antlr4" % "4.7.1",
        "com.typesafe.scala-logging" %% "scala-logging" % "3.9.0",
        "ch.qos.logback" % "logback-classic" % "1.2.3",
        "com.github.jnr" % "jnr-ffi" % "2.1.7",
        "com.github.jnr" % "jffi" % "1.2.16" classifier "native",
        "com.github.jnr" % "jnr-posix" % "3.0.44",
        "org.scalatest" %% "scalatest" % "3.0.5" % "test",
        "junit" % "junit" % "4.12" % "test"
    ),

    scalacOptions += "-language:implicitConversions",

    testOptions in Test += Tests.Argument("-oF"), // print full stack trace when testing

    parallelExecution in Test := false,  // disable parallel tests because the refimpl2 is not thread-safe

    antlr4PackageName in Antlr4 := Some("uvm.ir.textinput.gen"),

    antlr4GenListener in Antlr4 := false,

    antlr4GenVisitor in Antlr4 := false,

    antlr4Version in Antlr4 := "4.7.1",

    makeClasspathFile := {
      val cp = (fullClasspath in Runtime).value.files 

      println("fullClasspath: \n" + cp.mkString("\n"))

      val cpStr = cp.mkString(":")

      IO.write(new java.io.File("classpath.txt"), cpStr)
    }
  )
