"""
Usage: python3 irbuildernodestoirbuildermethods.py
"""

import re
import sys, os.path, io

import injecttools
from refimpl2injectablefiles import injectable_files, irbuilder_nodes_path

replaces = [(re.compile(x), y) for (x,y) in [
    (r'TrantientBundle', 'MuBundleNode'),
    (r'TB', 'MuBundleNode'),
    (r'Bundle', 'MuBundleNode'),
    (r'TopLevel', 'MuChildNode'),
    (r'ChildNode', 'MuChildNode'),
    (r'Type\w*', 'MuTypeNode'),
    (r'Abstract\w+Type', 'MuTypeNode'),
    (r'FuncSig', 'MuFuncSigNode'),
    (r'Const\w+', 'MuConstNode'),
    (r'GlobalCell', 'MuGlobalNode'),
    (r'Function', 'MuFuncNode'),
    (r'ExposedFunc', 'MuExpFuncNode'),
    (r'FuncVer', 'MuFuncVerNode'),
    (r'BasicBlock', 'MuBBNode'),
    (r'BB', 'MuBBNode'),
    (r'SSAVariable', 'MuVarNode'),
    (r'Var', 'MuVarNode'),
    (r'LocalVariable', 'MuLocalVarNode'),
    (r'NorParam', 'MuNorParamNode'),
    (r'ExcParam', 'MuExcParamNode'),
    (r'InstResult', 'MuInstResNode'),
    (r'Inst\w+', 'MuInstNode'),
    (r'HasKeepaliveClause', 'MuInstNode'),
    ]]

case_class_r = re.compile(r'^(case\s+class\s+(\w+)\s*\(([^)]*)\))', re.MULTILINE)
arg = re.compile(r'(\w+):\s*([a-zA-Z0-9\[\].]+)')

begin="EXT:BEGIN:IRBUILDER_NODES"
end="EXT:END:IRBUILDER_NODES"

def main():
    with open(irbuilder_nodes_path) as f:
        lines = f.read()

    text = injecttools.extract_lines(lines, begin, end)

    output = io.StringIO()

    for whole, name, arglist in case_class_r.findall(text):
        node_name = name
        func_name = "new" + name[4:]

        argnames = []
        argtypes = []

        for an,at in arg.findall(arglist):
            argnames.append(an)
            argtypes.append(at)

        print("  def {}({}): Unit = {{".format(func_name, ", ".join(
            "{}: {}".format(an, at) for an, at in zip(argnames, argtypes))),
            file=output)

        print("    val _node = new {}({})".format(node_name,
            ", ".join(argnames)), file=output)

        print("    onNewNodeCreated(_node)", file=output)
        print("  }", file=output)

    #print(output.getvalue())

    #return

    injectable_files["IRBuilder.scala"].inject_many({
        "IRBUILDERNODE_CONSTRUCTORS": output.getvalue(),
        })
            
if __name__=='__main__':
    main()

