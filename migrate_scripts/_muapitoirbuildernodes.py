""" This is for one-time use only. It generates contents of
uvm.ir.irbuilder/irBuilderNode.scala, but that file should be maintained
manually in the long run.  """

import muapiparser
from refimpl2injectablefiles import muapi_h_path
from muapitocstubs import toCamelCase

_to_scala_type = {
        "int": "Int",
        "MuBool": "Boolean",
        }

def to_scala_type(rt):
    return _to_scala_type.get(rt, rt)

def generate_node(method):
    func_name = toCamelCase(method["name"])
    node_name = "Node" + func_name[3:]

    params = method["params"][1:] # skip MuIRBuilder*
    node_params = []

    for param in params:
        pn = param['name']
        pt = param['type']
        nn = toCamelCase(pn)
        if param.get("is_sz_param", False):
            continue
        elif 'array_sz_param' in param:
            assert pt.endswith("*")
            rt = pt[:-1]
            st = to_scala_type(rt)
            nt = "Seq[{}]".format(pt[:-1])
        elif param.get('is_optional', False):
            assert pt in ["MuCString", "MuID"] or pt.endswith("*") or any(
                    pt.endswith(x) for x in ["Clause", "Node"]), pt
            if pt.endswith("*"):
                rt = pt[:-1]
            else:
                rt = pt
            st = to_scala_type(rt)
            nt = "Option[{}]".format(rt)
        else:
            rt = pt
            st = to_scala_type(rt)
            nt = st

        node_params.append((nn, nt))

    return '''case class {}({}) extends IRBuilderNode(id)'''.format(
            node_name, ", ".join("{}: {}".format(nn, nt) for (nn, nt) in node_params))

def generate_things(ast):
    irb = [s for s in ast["structs"] if s["name"] == "MuIRBuilder"][0]

    nodes = []

    for meth in irb["methods"]:
        if meth["name"].startswith("new_"):
            nodes.append(generate_node(meth))
        
    return "\n".join(nodes)
    

def main():
    with open(muapi_h_path) as f:
        src_text = f.read()

    ast = muapiparser.parse_muapi(src_text)

    generated = generate_things(ast)

    print(generated)

if __name__=='__main__':
    main()
