ifndef JAVA_HOME
    $(error JAVA_HOME is required. Invoke with 'make JAVA_HOME=/path/to/java/home')
endif

unamem := $(shell uname -m)

ifeq ($(unamem),x86_64)
    JDKARCH := amd64
else ifeq ($(unamem),aarch64)
    JDKARCH := aarch64
else
    $(error Unsupported architecture: $(unamem))
endif

CFLAGS += -std=gnu11 -g -I $(JAVA_HOME)/include

ifndef OS
    uname := $(shell uname)
	ifeq ($(uname),Darwin)
		OS = OSX
    else ifeq ($(uname),Linux)
		OS = LINUX
    else
		$(error Unrecognized operating system $(uname). I currently only worked on OSX and Linux.)
    endif
endif

ifeq ($(OS),OSX)
	CFLAGS += -I $(JAVA_HOME)/include/darwin
	LDFLAGS += -L $(JAVA_HOME)/lib/jli -l jli -rpath $(JAVA_HOME)/lib/jli -install_name '@rpath/libmurefimpl2start.so'
else ifeq ($(OS),LINUX)
	CFLAGS += -I $(JAVA_HOME)/include/linux
	LDFLAGS += -Wl,--no-as-needed -L $(JAVA_HOME)/jre/lib/$(JDKARCH)/server -l jvm -Wl,-rpath,$(JAVA_HOME)/jre/lib/$(JDKARCH)/server
endif

.PHONY: all
all: libs tests

ifeq ($(OS),OSX)
.PHONY: libs
libs: libmurefimpl2start.so libmurefimpl2start.dylib
else
.PHONY: libs
libs: libmurefimpl2start.so
endif

libmurefimpl2start.so: refimpl2-start.c classpath.h
	$(CC) -fPIC -shared $(CFLAGS) -o $@ $< $(LDFLAGS)

libmurefimpl2start.dylib:
	ln -s libmurefimpl2start.so libmurefimpl2start.dylib

../classpath.txt: ../build.sbt
	cd .. ; sbt makeClasspathFile

classpath.h: ../classpath.txt
	cp ../classpath.txt ./
	xxd -i classpath.txt ./classpath.h

.PHONY: tests
tests: test_client test_client2

test_client: test_client.c libmurefimpl2start.so
	$(CC) `./refimpl2-config --istart --cflags --libs` -o $@ $<

test_client2: test_client2.c libmurefimpl2start.so
	$(CC) `./refimpl2-config --istart --cflags --libs` -o $@ $<

.PHONY: clean veryclean
clean:
	rm -f *.so test_client test_client2 *.dylib

veryclean: clean
	rm -f classpath.txt classpath.h ../classpath.txt
