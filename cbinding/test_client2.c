#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

#include <unistd.h>     // write

#include <refimpl2-start.h>
#include <muapi.h>

const char *hw_string =
"**************\n"
"*Hello world!*\n"
"**************\n";

const char *gc_conf =
"sosSize=524288\n"
"losSize=524288\n"
"globalSize=1048576\n"
"stackSize=32768\n"
"vmLog=DEBUG\n"
;

char *boot_image_name = "test_client2_bootimg.mu";

int *muerrno;

#define MUCHECKERR() do { if(*muerrno) { \
        fprintf(stderr, "Line %d: Error thrown in Mu. Stop. **muerrno: %d\n", \
                __LINE__, *muerrno); \
        exit(1); \
    }} while(0)

#define SYM(i) MuID i = b->gen_sym(b, "@" #i)

// Some wrappers for making comminsts.

void make_pin(MuIRBuilder *b, MuID id, MuID result_id,
        MuTypeNode ty, MuVarNode ref) {
    MuID results[]   = { result_id };
    MuID type_args[] = { ty };
    MuID value_args[] = { ref };
    b->new_comminst(b, id, results, 1,
            MU_CI_UVM_NATIVE_PIN,
            NULL, 0, type_args, 1,
            NULL, 0, value_args, 1,
            MU_NO_ID, MU_NO_ID);
}

void make_unpin(MuIRBuilder *b, MuID id,
        MuTypeNode ty, MuVarNode ref) {
    MuID type_args[] = { ty };
    MuID value_args[] = { ref };
    b->new_comminst(b, id, NULL, 0,
            MU_CI_UVM_NATIVE_UNPIN,
            NULL, 0, type_args, 1,
            NULL, 0, value_args, 1,
            MU_NO_ID, MU_NO_ID);
}

int main(int argc, char** argv) {
    MuVM *mvm = mu_refimpl2_new_ex(gc_conf);

    muerrno = mvm->get_mu_error_ptr(mvm);

    MuCtx *ctx = mvm->new_context(mvm);
    
    MUCHECKERR();

    MuIRBuilder *b = ctx->new_ir_builder(ctx);

    // primitive types
    
    MuID i1   = b->gen_sym(b, "@i1");   // The raw form

    SYM(i8);    // My convenient macro :)
    SYM(i32);
    SYM(i64);
    SYM(pi8);
    SYM(ppi8);

    //  typedef uint8_t i8;
    //  ...
    b->new_type_int(b, i1, 1);
    b->new_type_int(b, i8, 8);
    b->new_type_int(b, i32, 32);
    b->new_type_int(b, i64, 64);
    b->new_type_uptr(b, pi8, i8);
    b->new_type_uptr(b, ppi8, pi8);

    //  typedef struct { i8 var_part[]; } string_t;
    //  typedef ref<string_t> string_r;
    SYM(string_t);
    SYM(string_r);
    b->new_type_hybrid(b, string_t, NULL, 0, i8);
    b->new_type_ref(b, string_r, string_t);

    //  static string_r hw_string;
    SYM(g_hello_world);
    b->new_global_cell(b, g_hello_world, string_r);

    // signature for the main function
    
    //  typedef void (main_sig)(i32, ppi8);
    SYM(main_sig);
    MuTypeNode    main_paramtys[] = { i32, ppi8 };
    b->new_funcsig(b, main_sig, main_paramtys, 2, NULL, 0);

    // signature and ufuncptr for the write function
    
    //  typedef void *pvoid;
    SYM(muvoid);
    SYM(pvoid);
    
    b->new_type_void(b, muvoid);
    b->new_type_uptr(b, pvoid, muvoid);

    //  typedef i64 (*write_fp)(i32, pvoid, i64);
    SYM(write_sig);
    SYM(write_fp);

    MuTypeNode    write_paramtys[] = { i32, pvoid, i64 };
    MuTypeNode    write_rettys[]   = { i64 };
    b->new_funcsig(b, write_sig, write_paramtys, 3, write_rettys, 1);
    b->new_type_ufuncptr(b, write_fp, write_sig);

    //  static write_fp const_fp_write = (uintptr_t)write;
    SYM(const_fp_write);
    b->new_const_extern(b, const_fp_write, write_fp, "write"); // external linkage

    // the exit state of the Mu function. It cannot directly return to C.

    SYM(exit_status);

    b->new_global_cell(b, exit_status, i32);

    // the main function

    SYM(main_func);

    b->new_func(b, main_func, main_sig);

    // the main function body

    //  void entry(i32 p_argc, ppi8 p_argv) {

    SYM(p_argc);
    SYM(p_argv);
    
    // args to write. fd=1, sz=size of hw_string
    //      const i32 const_i32_1 = 1;
    //      const i64 const_i64_hwsz = strlen(hw_string)+1;
    SYM(const_i32_1);
    SYM(const_i64_hwsz);
    b->new_const_int(b, const_i32_1, i32, 1L);
    b->new_const_int(b, const_i64_hwsz, i64, strlen(hw_string)+1);

    //      hw_ref = g_hello_world;
    SYM(inst_load);
    SYM(hw_ref);
    b->new_load(b, inst_load, hw_ref, 0, MU_ORD_NOT_ATOMIC, string_r, g_hello_world, MU_NO_ID);

    //      hw_ptr = pin(hw_ref);
    SYM(inst_pin);
    SYM(hw_ptr);
    make_pin(b, inst_pin, hw_ptr, string_r, hw_ref);

    //      var_x = (ppi8)hw_ptr;  // inst_ptrcast
    SYM(inst_ptrcast);
    SYM(var_x);
    b->new_conv(b, inst_ptrcast, var_x, MU_CONV_PTRCAST,
            ppi8, pvoid, hw_ptr);

    //      var_y = write(const_i32_1, var_x, const_i64_hwsz);  // inst_write
    SYM(var_y);
    SYM(inst_write);
    MuVarNode      write_args[] = { const_i32_1, var_x, const_i64_hwsz };
    MuVarNode      write_rvs[]  = { var_y };
    b->new_ccall(b, inst_write, write_rvs, 1, MU_CC_DEFAULT,
            write_fp, write_sig, const_fp_write, write_args, 3, MU_NO_ID, MU_NO_ID);

    //      unpin(hw_ref);
    SYM(inst_unpin);
    make_unpin(b, inst_unpin, string_r, hw_ref);

    //      const i32 const_i32_0 = 0;
    //      *exit_status = const_i32_0;         // inst_store
    SYM(const_i32_0);
    SYM(inst_store);
    b->new_const_int(b, const_i32_0, i32, 0L);
    b->new_store(b, inst_store, 0, MU_ORD_NOT_ATOMIC, i32,
            exit_status, const_i32_0, MU_NO_ID);

    // thread_exit
    //      uvm.thread_exit();                  // inst_threadexit
    SYM(inst_threadexit);
    b->new_comminst(b, inst_threadexit, NULL, 0, MU_CI_UVM_THREAD_EXIT,
            NULL, 0, NULL, 0, NULL, 0, NULL, 0, MU_NO_ID, MU_NO_ID);

    // Now construct the entry block
    SYM(entry);
    MuVarNode   entry_args[]   = { p_argc, p_argv };
    MuTypeNode  entry_argtys[] = { i32, ppi8 };
    MuInstNode  entry_insts[]  = { inst_load, inst_pin, inst_ptrcast,
        inst_write, inst_unpin, inst_store, inst_threadexit };
    b->new_bb(b, entry, entry_args, entry_argtys, 2, MU_NO_ID, entry_insts, 
            sizeof(entry_insts)/sizeof(entry_insts[0]));

    // and the function version (function body)

    SYM(main_fv);
    MuBBNode    main_fv_bbs[] = { entry };
    b->new_func_ver(b, main_fv, main_func, main_fv_bbs, 1);

    b->load(b);

    MUCHECKERR();

    // Allocate an object to hold the "hello world" string.
    MuIntValue v_hw_sz  = ctx->handle_from_sint64(ctx, strlen(hw_string), 64);
    MuRefValue v_hw_obj = ctx->new_hybrid(ctx, string_t, v_hw_sz);

    // Populate the content.
    MuUPtrValue v_hw_uptr = ctx->pin(ctx, v_hw_obj);
    char *hw_uptr = (char*)ctx->handle_to_ptr(ctx, v_hw_uptr);
    strcpy(hw_uptr, hw_string);
    ctx->unpin(ctx, v_hw_obj);

    // Set the global cell
    MuIRefValue v_g_hello_world = ctx->handle_from_global(ctx, g_hello_world);
    ctx->store(ctx, MU_ORD_NOT_ATOMIC, v_g_hello_world, v_hw_obj);

    MuFuncRefValue   v_main_func   = ctx->handle_from_func(ctx, main_func);

    // Not used anywhere. Just use this to test thread-local objref.
    MuIntValue v_main_thread_local = ctx->new_fixed(ctx, i64);

    printf("Making boot image...\n");

    // I just want i64 to be in the boot image, too, in addition to the main funciton.
    MuID whitelist[] = { i64, main_func };
    ctx->make_boot_image(ctx,
            whitelist, 2,
            v_main_func, NULL, v_main_thread_local,
            NULL, NULL, 0,
            NULL, NULL, 0,
            boot_image_name);

    printf("Boot image created. Use the following command to run:\n");
    printf("  ../tools/runmu.sh ./%s\n", boot_image_name);

    MuIntValue  v_argc = ctx->handle_from_sint32(ctx, i32,  argc);
    MuUPtrValue v_argv = ctx->handle_from_ptr(ctx, ppi8, (MuCPtr)argv);

    MuValue v_main_args[] = {v_argc, v_argv};

    MuStackRefValue  v_main_stack  = ctx->new_stack(ctx, v_main_func);
    MuThreadRefValue v_main_thread = ctx->new_thread_nor(ctx, v_main_stack,
            NULL, v_main_args, 2);

    MUCHECKERR();

    mvm->execute(mvm);

    mu_refimpl2_close(mvm);

    return 0;
}
