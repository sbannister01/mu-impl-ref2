#!/bin/bash

MYDIR=$(dirname $0)
CLASSPATH_TXT=${MYDIR}/../classpath.txt

if [[ ! -f ${CLASSPATH_TXT} ]]; then
    echo "classpath.txt does not exist. Creating..."
    pushd ..
    sbt makeClasspathFile
    popd
fi

exec java -Xmx4096M -cp $(< ${CLASSPATH_TXT}) uvm.refimpl.cmdline.RunMu "$@"
