package uvm.staticanalysis

import uvm.types._
import uvm.ssavariables._

object InstructionResultInferer {
  def inferInstResultNum(inst: Instruction): Int = inst match {
    case i: InstBinOp => 1 + BinOpStatus.numOfFlags(i.flags)
    case i: InstCmp => 1
    case i: InstConv => 1
    case i: InstSelect => 1
    case i: InstBranch => 0
    case i: InstBranch2 => 0
    case i: InstSwitch => 0
    case i: InstCall => i.sig.retTys.size
    case i: InstTailCall => 0
    case i: InstRet => 0
    case i: InstThrow => 0
    case i: InstExtractValue => 1
    case i: InstInsertValue => 1
    case i: InstExtractElement => 1
    case i: InstInsertElement => 1
    case i: InstShuffleVector => 1
    case i: InstNew => 1
    case i: InstNewHybrid => 1
    case i: InstAlloca => 1
    case i: InstAllocaHybrid => 1
    case i: InstGetIRef => 1
    case i: InstGetFieldIRef => 1
    case i: InstGetElemIRef => 1
    case i: InstShiftIRef => 1
    case i: InstGetVarPartIRef => 1
    case i: InstLoad => 1
    case i: InstStore => 0
    case i: InstCmpXchg => 2
    case i: InstAtomicRMW => 1
    case i: InstFence => 0
    case i: InstTrap => i.retTys.size
    case i: InstWatchPoint => i.retTys.size
    case i: InstCCall => i.sig.retTys.size
    case i: InstNewThread => 1
    case i: InstSwapStack => i.curStackAction match {
      case RetWith(t) => t.size
      case _: KillOld => 0
    }
    case i: InstCommInst => i.inst.name.get match {
      case "@uvm.new_stack" => 1
      case "@uvm.kill_stack" => 0
      case "@uvm.thread_exit" => 0
      case "@uvm.current_stack" => 1
      case "@uvm.set_threadlocal" => 0
      case "@uvm.get_threadlocal" => 1
      case "@uvm.tr64.is_fp" => 1
      case "@uvm.tr64.is_int" => 1
      case "@uvm.tr64.is_ref" => 1
      case "@uvm.tr64.from_fp" => 1
      case "@uvm.tr64.from_int" => 1
      case "@uvm.tr64.from_ref" => 1
      case "@uvm.tr64.to_fp" => 1
      case "@uvm.tr64.to_int" => 1
      case "@uvm.tr64.to_ref" => 1
      case "@uvm.tr64.to_tag" => 1
      case "@uvm.futex.wait" => 1
      case "@uvm.futex.wait_timeout" => 1
      case "@uvm.futex.wake" => 1
      case "@uvm.futex.cmp_requeue" => 1
      case "@uvm.kill_dependency" => 1
      case "@uvm.native.pin" => 1
      case "@uvm.native.unpin" => 0
      case "@uvm.native.expose" => 1
      case "@uvm.native.unexpose" => 0
      case "@uvm.native.get_cookie" => 1

      case "@uvm.meta.id_of" => 1
      case "@uvm.meta.name_of" => 1
      case "@uvm.meta.load_bundle" => 1
      case "@uvm.meta.load_hail" => 1

      case "@uvm.meta.new_cursor" => 1
      case "@uvm.meta.next_frame" => 0
      case "@uvm.meta.copy_cursor" => 1
      case "@uvm.meta.close_cursor" => 0
      
      case "@uvm.meta.cur_func" => 1
      case "@uvm.meta.cur_func_ver" => 1
      case "@uvm.meta.cur_inst" => 1
      case "@uvm.meta.dump_keepalives" => 1

      case "@uvm.meta.pop_frames_to" => 0
      case "@uvm.meta.push_frame" => 0

      case "@uvm.meta.enable_watchPoint" => 0
      case "@uvm.meta.disable_watchPoint" => 0

      case "@uvm.meta.set_trap_handler" => 0
      
      /// GEN:BEGIN:IRBUILDER_RETVAL_NUMS
      case "@uvm.irbuilder.load" => 0
      case "@uvm.irbuilder.abort" => 0
      case "@uvm.irbuilder.gen_sym" => 1
      case "@uvm.irbuilder.new_type_int" => 0
      case "@uvm.irbuilder.new_type_float" => 0
      case "@uvm.irbuilder.new_type_double" => 0
      case "@uvm.irbuilder.new_type_uptr" => 0
      case "@uvm.irbuilder.new_type_ufuncptr" => 0
      case "@uvm.irbuilder.new_type_struct" => 0
      case "@uvm.irbuilder.new_type_hybrid" => 0
      case "@uvm.irbuilder.new_type_array" => 0
      case "@uvm.irbuilder.new_type_vector" => 0
      case "@uvm.irbuilder.new_type_void" => 0
      case "@uvm.irbuilder.new_type_ref" => 0
      case "@uvm.irbuilder.new_type_iref" => 0
      case "@uvm.irbuilder.new_type_weakref" => 0
      case "@uvm.irbuilder.new_type_funcref" => 0
      case "@uvm.irbuilder.new_type_tagref64" => 0
      case "@uvm.irbuilder.new_type_threadref" => 0
      case "@uvm.irbuilder.new_type_stackref" => 0
      case "@uvm.irbuilder.new_type_framecursorref" => 0
      case "@uvm.irbuilder.new_type_irbuilderref" => 0
      case "@uvm.irbuilder.new_funcsig" => 0
      case "@uvm.irbuilder.new_const_int" => 0
      case "@uvm.irbuilder.new_const_int_ex" => 0
      case "@uvm.irbuilder.new_const_float" => 0
      case "@uvm.irbuilder.new_const_double" => 0
      case "@uvm.irbuilder.new_const_null" => 0
      case "@uvm.irbuilder.new_const_seq" => 0
      case "@uvm.irbuilder.new_const_extern" => 0
      case "@uvm.irbuilder.new_global_cell" => 0
      case "@uvm.irbuilder.new_func" => 0
      case "@uvm.irbuilder.new_exp_func" => 0
      case "@uvm.irbuilder.new_func_ver" => 0
      case "@uvm.irbuilder.new_bb" => 0
      case "@uvm.irbuilder.new_dest_clause" => 0
      case "@uvm.irbuilder.new_exc_clause" => 0
      case "@uvm.irbuilder.new_keepalive_clause" => 0
      case "@uvm.irbuilder.new_csc_ret_with" => 0
      case "@uvm.irbuilder.new_csc_kill_old" => 0
      case "@uvm.irbuilder.new_nsc_pass_values" => 0
      case "@uvm.irbuilder.new_nsc_throw_exc" => 0
      case "@uvm.irbuilder.new_binop" => 0
      case "@uvm.irbuilder.new_binop_with_status" => 0
      case "@uvm.irbuilder.new_cmp" => 0
      case "@uvm.irbuilder.new_conv" => 0
      case "@uvm.irbuilder.new_select" => 0
      case "@uvm.irbuilder.new_branch" => 0
      case "@uvm.irbuilder.new_branch2" => 0
      case "@uvm.irbuilder.new_switch" => 0
      case "@uvm.irbuilder.new_call" => 0
      case "@uvm.irbuilder.new_tailcall" => 0
      case "@uvm.irbuilder.new_ret" => 0
      case "@uvm.irbuilder.new_throw" => 0
      case "@uvm.irbuilder.new_extractvalue" => 0
      case "@uvm.irbuilder.new_insertvalue" => 0
      case "@uvm.irbuilder.new_extractelement" => 0
      case "@uvm.irbuilder.new_insertelement" => 0
      case "@uvm.irbuilder.new_shufflevector" => 0
      case "@uvm.irbuilder.new_new" => 0
      case "@uvm.irbuilder.new_newhybrid" => 0
      case "@uvm.irbuilder.new_alloca" => 0
      case "@uvm.irbuilder.new_allocahybrid" => 0
      case "@uvm.irbuilder.new_getiref" => 0
      case "@uvm.irbuilder.new_getfieldiref" => 0
      case "@uvm.irbuilder.new_getelemiref" => 0
      case "@uvm.irbuilder.new_shiftiref" => 0
      case "@uvm.irbuilder.new_getvarpartiref" => 0
      case "@uvm.irbuilder.new_load" => 0
      case "@uvm.irbuilder.new_store" => 0
      case "@uvm.irbuilder.new_cmpxchg" => 0
      case "@uvm.irbuilder.new_atomicrmw" => 0
      case "@uvm.irbuilder.new_fence" => 0
      case "@uvm.irbuilder.new_trap" => 0
      case "@uvm.irbuilder.new_watchpoint" => 0
      case "@uvm.irbuilder.new_wpbranch" => 0
      case "@uvm.irbuilder.new_ccall" => 0
      case "@uvm.irbuilder.new_newthread" => 0
      case "@uvm.irbuilder.new_swapstack" => 0
      case "@uvm.irbuilder.new_comminst" => 0
      /// GEN:END:IRBUILDER_RETVAL_NUMS
    }
  }
}
