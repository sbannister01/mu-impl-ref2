package uvm.staticanalysis

import uvm.types._
import uvm.utils.LazyPool

class InternalEntityPool {
  val intOf = LazyPool(TypeInt)
  val refOf = LazyPool(TypeRef)
  val irefOf = LazyPool(TypeIRef)
  val ptrOf = LazyPool(TypeUPtr)
  val funcOf = LazyPool(TypeFuncRef)
  val funcPtrOf = LazyPool(TypeUFuncPtr)
  val vecOf = LazyPool[(Type, Long), TypeVector] { case (t, l) => TypeVector(t, l) }
  def unmarkedOf(t: Type): Type = t match {
    case TypeWeakRef(r) => refOf(r)
    case _ => t
  }
}