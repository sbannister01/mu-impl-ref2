package uvm.staticanalysis

import uvm.UvmException

class StaticAnalyserException(message: String = null, cause: Throwable = null) extends UvmException(message, cause)