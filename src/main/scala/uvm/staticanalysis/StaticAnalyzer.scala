package uvm.staticanalysis

import uvm.utils.IDFactory

class StaticAnalyzer {
  val internalIDFactory = IDFactory.newPrivateIDFactory()
  
  val predefinedEntities = new PredefinedEntities(internalIDFactory)
  val internalEntityPool = new InternalEntityPool()
  val typeInferer = new TypeInferer(predefinedEntities, internalEntityPool)
  
  val bundleChecker = new BundleChecker()
}