package uvm.staticanalysis

import uvm._
import uvm.ssavariables._
import uvm.types._
import uvm.utils.IDFactory

class PredefinedEntities(privateIDFactory: IDFactory) {
  import uvm.RichIdentifiedSettable._

  def internal(name: MuName): (MuID, MuName) = {
    val id = privateIDFactory.getID()
    val n = "@uvm.internal.types." + name
    (id, n)
  }
  
  val C_INT = TypeInt(32) := internal("c_int")
  val C_CHAR = TypeInt(8) := internal("c_char")
  val C_CHARP = TypeUPtr(C_CHAR) := internal("c_charp")
  val C_CHARPP = TypeUPtr(C_CHARP) := internal("c_charpp")

  val I1 = TypeInt(1) := internal("i1")
  val I6 = TypeInt(6) := internal("i6")
  val I8 = TypeInt(6) := internal("i8")
  val I16 = TypeInt(6) := internal("i16")
  val I32 = TypeInt(32) := internal("i32")
  val I52 = TypeInt(52) := internal("i52")
  val I64 = TypeInt(52) := internal("i64")
  val FLOAT = TypeFloat() := internal("float")
  val DOUBLE = TypeDouble() := internal("double")
  val VOID = TypeVoid() := internal("void")

  val BYTE = TypeInt(8) := internal("byte")
  val BYTE_ARRAY = TypeHybrid(Seq(), BYTE) := internal("byte_array")

  val REF_VOID = TypeRef(VOID) := internal("ref_void")
  val IREF_VOID = TypeIRef(VOID) := internal("iref_void")

  val SIG_VV = FuncSig(Seq(), Seq()) := internal("sig_vv")
  val FUNCREF_VV = TypeFuncRef(SIG_VV) := internal("funcref_vv")
  
  val STACKREF = TypeStackRef() := internal("stackref")
  val THREADREF = TypeThreadRef() := internal("threadref")
  val FRAMECURSORREF = TypeFrameCursorRef() := internal("framecursorref")
  val IRBUILDERREF = TypeIRBuilderRef() := internal("irbuilderref")
  val TAGREF64 = TypeTagRef64() := internal("tagref64")

  val BYTES = TypeHybrid(Seq(I64), I8) := (0x260, "@uvm.meta.bytes")
  val BYTES_R = TypeRef(BYTES) := (0x261, "@uvm.meta.bytes_r")
  val REFS = TypeHybrid(Seq(I64), REF_VOID) := (0x262, "@uvm.meta.refs")
  val REFS_R = TypeRef(BYTES) := (0x263, "@uvm.meta.refs_r")

  val NULL_REF_VOID = ConstNull(REF_VOID) := internal("null_ref_void")
  val NULL_IREF_VOID = ConstNull(IREF_VOID) := internal("null_iref_void")
  val NULL_FUNCREF_VV = ConstNull(FUNCREF_VV) := internal("null_funcref_vv")
  val NULL_THREADREF = ConstNull(THREADREF) := internal("null_threadref")
  val NULL_STACKREF = ConstNull(STACKREF) := internal("null_stackref")
  
}