package uvm.ir.irbuilder

import uvm.UvmException
import uvm.MuID
import uvm.MuName

/** Base class for exceptions thrown by the IR builder. */
class IRBuilderException(message: String = null, cause: Throwable = null) extends UvmException(message, cause)

/**
 * Thrown if the Mu name provided to setName is illegal
 */
class IllegalNameException(message: String = null, cause: Throwable = null) extends IRBuilderException(message, cause)

/**
 * Thrown if the external symbol provided to new_const_extern is illegal
 */
class IllegalSymbolException(message: String = null, cause: Throwable = null) extends IRBuilderException(message, cause)

/**
 * Thrown if an ID does not correspond to any IR nodes
 */
class NodeIDNotFoundException(id: MuID, name: Option[MuName], cause: Throwable = null) extends {
  private val msg = ("Node id:%d, name:%s not found. " +
    "Chance is that you created this ID using gen_sym, but did not define the actual node.").format(
      id, name.getOrElse("(no name)"))
} with IRBuilderException(msg, cause)

/**
 * Thrown if a MuID cannot be resolved
 */
class UnknownIDException(id: MuID, name: Option[MuName], cause: Throwable = null) extends {
  private val msg = ("MuID %d (name: %s) is not defined. " +
    "If your Mu IR bundle refers to previously loaded entities, make sure the IDs are correct. " +
    "If it refers to entities in the current bundle, make sure they have the appropriate types. " +
    "Also check if all instructions in a basic block are listed in the new_bb's arguments.").format(
      id, name.getOrElse("(no name)"))
} with IRBuilderException(msg, cause)
