package uvm.ir.irbuilder

import scala.collection.mutable.ArrayBuffer
import scala.collection.mutable.HashMap

import uvm._
import uvm.comminsts.CommInst
import uvm.refimpl.MuInternalID
import uvm.refimpl.integerize.HasID
import uvm.ssavariables._
import uvm.types._
import uvm.utils.IDFactory

object IRBuilder {

  val validChars = {
    val table = new Array[Boolean](256)
    for (ch <- 'A' to 'Z') { table(ch) = true }
    for (ch <- 'a' to 'z') { table(ch) = true }
    for (ch <- '0' to '9') { table(ch) = true }
    for (ch <- "_-.") { table(ch) = true }
    table
  }

  val validSymbols = {
    val table = new Array[Boolean](256)
    for (ch <- 32 to 126) { table(ch) = true }
    table('"') = false
    table
  }

  def validateName(name: String): MuName = {
    if (name.startsWith("%")) {
      throw new IllegalNameException("Name must be global (starting with '@') when using the IR building API. Found: %s".format(name))
    } else if (!name.startsWith("@")) {
      throw new IllegalNameException("Name must start with '@'. Found: %s".format(name))
    } else {
      for (ch <- name.substring(1) if !validChars(ch)) {
        throw new IllegalNameException("'%c' is not a valid character for Mu name. Name: %s".format(ch, name))
      }
    }

    name
  }

  def validateSymbol(sym: String): String = {
    for (ch <- sym if !validSymbols(ch)) {
      throw new IllegalSymbolException("'%c' is not a valid character for external symbol. Symbol: %s".format(ch, sym))
    }

    sym
  }

}

trait IRBuilderListener {
  def onBundleLoad(irBuilder: IRBuilder, bundle: TrantientBundle): Unit
  def onBundleAbort(irBuilder: IRBuilder): Unit
}

/** The client-visible API for constructing Mu IR bundles. */
class IRBuilder(val id: MuInternalID,
    globalBundle: GlobalBundle, idFactory: IDFactory, irBuilderListener: Option[IRBuilderListener])
    extends HasID {
  import IRBuilder._
  import IRBuilderNode._

  private def nextID(): Int = idFactory.getID()
  
  private val idNameMap = new HashMap[MuID, MuName]()
  private val nodeList = new ArrayBuffer[IRBuilderNode]()
  
  private def onNewNodeCreated(node: IRBuilderNode): Unit = {
    nodeList += node
  }
  
  def makeBundle(): TrantientBundle = {
    val bc = new BundleConstructor(idNameMap.toMap, nodeList, globalBundle)
    bc.makeBundle()
  }
  
  def load(): Unit = {
    val trantientBundle = makeBundle()
    irBuilderListener.foreach { l => l.onBundleLoad(this, trantientBundle) }
  }
  
  def abort(): Unit = {
    irBuilderListener.foreach { l => l.onBundleAbort(this) }
  }

  def genSym(name: Option[String]): MuID = {
    val id = nextID()
    name foreach { n =>
      idNameMap(id) = validateName(n)
    }
    id
  }
  
  // Convenient functions for Scala
  def genSym(): MuID = genSym(None)
  def genSym(name: String): MuID = genSym(Some(name))
  

  // Special cases
  def newBinOp(id: MuID, resultID: MuID, optr: BinOptr.Value, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    newBinOp(id, resultID, Seq(), optr, 0, ty, opnd1, opnd2, excClause)
  }

  def newBinOpWithStatus(id: MuID, resultID: MuID, statusResultIDs: Seq[MuID], optr: BinOptr.Value, flags: BinOpStatus, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    newBinOp(id, resultID, statusResultIDs, optr, flags, ty, opnd1, opnd2, excClause)
  }

  
  // The following functions are generated from irBuilderNodes.scala by /migrate_scripts/irbuildernodestoirbuildermethods.py
  
  // GEN:BEGIN:IRBUILDERNODE_CONSTRUCTORS
  def newTypeInt(id: MuID, len: Int): Unit = {
    val _node = new NodeTypeInt(id, len)
    onNewNodeCreated(_node)
  }
  def newTypeFloat(id: MuID): Unit = {
    val _node = new NodeTypeFloat(id)
    onNewNodeCreated(_node)
  }
  def newTypeDouble(id: MuID): Unit = {
    val _node = new NodeTypeDouble(id)
    onNewNodeCreated(_node)
  }
  def newTypeUPtr(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeTypeUPtr(id, ty)
    onNewNodeCreated(_node)
  }
  def newTypeUFuncPtr(id: MuID, sig: MuFuncSigNode): Unit = {
    val _node = new NodeTypeUFuncPtr(id, sig)
    onNewNodeCreated(_node)
  }
  def newTypeStruct(id: MuID, fieldTys: Seq[MuTypeNode]): Unit = {
    val _node = new NodeTypeStruct(id, fieldTys)
    onNewNodeCreated(_node)
  }
  def newTypeHybrid(id: MuID, fixedTys: Seq[MuTypeNode], varTy: MuTypeNode): Unit = {
    val _node = new NodeTypeHybrid(id, fixedTys, varTy)
    onNewNodeCreated(_node)
  }
  def newTypeArray(id: MuID, elemTy: MuTypeNode, len: Long): Unit = {
    val _node = new NodeTypeArray(id, elemTy, len)
    onNewNodeCreated(_node)
  }
  def newTypeVector(id: MuID, elemTy: MuTypeNode, len: Long): Unit = {
    val _node = new NodeTypeVector(id, elemTy, len)
    onNewNodeCreated(_node)
  }
  def newTypeVoid(id: MuID): Unit = {
    val _node = new NodeTypeVoid(id)
    onNewNodeCreated(_node)
  }
  def newTypeRef(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeTypeRef(id, ty)
    onNewNodeCreated(_node)
  }
  def newTypeIRef(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeTypeIRef(id, ty)
    onNewNodeCreated(_node)
  }
  def newTypeWeakRef(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeTypeWeakRef(id, ty)
    onNewNodeCreated(_node)
  }
  def newTypeFuncRef(id: MuID, sig: MuFuncSigNode): Unit = {
    val _node = new NodeTypeFuncRef(id, sig)
    onNewNodeCreated(_node)
  }
  def newTypeTagRef64(id: MuID): Unit = {
    val _node = new NodeTypeTagRef64(id)
    onNewNodeCreated(_node)
  }
  def newTypeThreadRef(id: MuID): Unit = {
    val _node = new NodeTypeThreadRef(id)
    onNewNodeCreated(_node)
  }
  def newTypeStackRef(id: MuID): Unit = {
    val _node = new NodeTypeStackRef(id)
    onNewNodeCreated(_node)
  }
  def newTypeFrameCursorRef(id: MuID): Unit = {
    val _node = new NodeTypeFrameCursorRef(id)
    onNewNodeCreated(_node)
  }
  def newTypeIRBuilderRef(id: MuID): Unit = {
    val _node = new NodeTypeIRBuilderRef(id)
    onNewNodeCreated(_node)
  }
  def newFuncSig(id: MuID, paramTys: Seq[MuTypeNode], retTys: Seq[MuTypeNode]): Unit = {
    val _node = new NodeFuncSig(id, paramTys, retTys)
    onNewNodeCreated(_node)
  }
  def newConstInt(id: MuID, ty: MuTypeNode, value: BigInt): Unit = {
    val _node = new NodeConstInt(id, ty, value)
    onNewNodeCreated(_node)
  }
  def newConstFloat(id: MuID, ty: MuTypeNode, value: Float): Unit = {
    val _node = new NodeConstFloat(id, ty, value)
    onNewNodeCreated(_node)
  }
  def newConstDouble(id: MuID, ty: MuTypeNode, value: Double): Unit = {
    val _node = new NodeConstDouble(id, ty, value)
    onNewNodeCreated(_node)
  }
  def newConstNull(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeConstNull(id, ty)
    onNewNodeCreated(_node)
  }
  def newConstSeq(id: MuID, ty: MuTypeNode, elems: Seq[MuGlobalVarNode]): Unit = {
    val _node = new NodeConstSeq(id, ty, elems)
    onNewNodeCreated(_node)
  }
  def newConstExtern(id: MuID, ty: MuTypeNode, symbol: String): Unit = {
    val _node = new NodeConstExtern(id, ty, symbol)
    onNewNodeCreated(_node)
  }
  def newGlobalCell(id: MuID, ty: MuTypeNode): Unit = {
    val _node = new NodeGlobalCell(id, ty)
    onNewNodeCreated(_node)
  }
  def newFunc(id: MuID, sig: MuFuncSigNode): Unit = {
    val _node = new NodeFunc(id, sig)
    onNewNodeCreated(_node)
  }
  def newExpFunc(id: MuID, func: MuFuncNode, callconv: Flag, cookie: MuConstIntNode): Unit = {
    val _node = new NodeExpFunc(id, func, callconv, cookie)
    onNewNodeCreated(_node)
  }
  def newFuncVer(id: MuID, func: MuFuncNode, bbs: Seq[MuBBNode]): Unit = {
    val _node = new NodeFuncVer(id, func, bbs)
    onNewNodeCreated(_node)
  }
  def newBB(id: MuID, norParamIDs: Seq[MuID], norParamTys: Seq[MuTypeNode], excParamID: Option[MuID], insts: Seq[MuInstNode]): Unit = {
    val _node = new NodeBB(id, norParamIDs, norParamTys, excParamID, insts)
    onNewNodeCreated(_node)
  }
  def newDestClause(id: MuID, dest: MuBBNode, vars: Seq[MuVarNode]): Unit = {
    val _node = new NodeDestClause(id, dest, vars)
    onNewNodeCreated(_node)
  }
  def newExcClause(id: MuID, nor: MuDestClause, exc: MuDestClause): Unit = {
    val _node = new NodeExcClause(id, nor, exc)
    onNewNodeCreated(_node)
  }
  def newKeepaliveClause(id: MuID, vars: Seq[MuLocalVarNode]): Unit = {
    val _node = new NodeKeepaliveClause(id, vars)
    onNewNodeCreated(_node)
  }
  def newCscRetWith(id: MuID, rettys: Seq[MuVarNode]): Unit = {
    val _node = new NodeCscRetWith(id, rettys)
    onNewNodeCreated(_node)
  }
  def newCscKillOld(id: MuID): Unit = {
    val _node = new NodeCscKillOld(id)
    onNewNodeCreated(_node)
  }
  def newNscPassValues(id: MuID, tys: Seq[MuTypeNode], vars: Seq[MuVarNode]): Unit = {
    val _node = new NodeNscPassValues(id, tys, vars)
    onNewNodeCreated(_node)
  }
  def newNscThrowExc(id: MuID, exc: MuVarNode): Unit = {
    val _node = new NodeNscThrowExc(id, exc)
    onNewNodeCreated(_node)
  }
  def newBinOp(id: MuID, resultID: MuID, statusResultIDs: Seq[MuID], optr: BinOptr.Value, flags: BinOpStatus, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeBinOp(id, resultID, statusResultIDs, optr, flags, ty, opnd1, opnd2, excClause)
    onNewNodeCreated(_node)
  }
  def newCmp(id: MuID, resultID: MuID, optr: CmpOptr.Value, ty: MuTypeNode, opnd1: MuVarNode, opnd2: MuVarNode): Unit = {
    val _node = new NodeCmp(id, resultID, optr, ty, opnd1, opnd2)
    onNewNodeCreated(_node)
  }
  def newConv(id: MuID, resultID: MuID, optr: ConvOptr.Value, fromTy: MuTypeNode, toTy: MuTypeNode, opnd: MuVarNode): Unit = {
    val _node = new NodeConv(id, resultID, optr, fromTy, toTy, opnd)
    onNewNodeCreated(_node)
  }
  def newSelect(id: MuID, resultID: MuID, condTy: MuTypeNode, opndTy: MuTypeNode, cond: MuVarNode, ifTrue: MuVarNode, ifFalse: MuVarNode): Unit = {
    val _node = new NodeSelect(id, resultID, condTy, opndTy, cond, ifTrue, ifFalse)
    onNewNodeCreated(_node)
  }
  def newBranch(id: MuID, dest: MuDestClause): Unit = {
    val _node = new NodeBranch(id, dest)
    onNewNodeCreated(_node)
  }
  def newBranch2(id: MuID, cond: MuVarNode, ifTrue: MuDestClause, ifFalse: MuDestClause): Unit = {
    val _node = new NodeBranch2(id, cond, ifTrue, ifFalse)
    onNewNodeCreated(_node)
  }
  def newSwitch(id: MuID, opndTy: MuTypeNode, opnd: MuVarNode, defaultDest: MuDestClause, cases: Seq[MuConstNode], dests: Seq[MuDestClause]): Unit = {
    val _node = new NodeSwitch(id, opndTy, opnd, defaultDest, cases, dests)
    onNewNodeCreated(_node)
  }
  def newCall(id: MuID, resultIDs: Seq[MuID], sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeCall(id, resultIDs, sig, callee, args, excClause, keepaliveClause)
    onNewNodeCreated(_node)
  }
  def newTailCall(id: MuID, sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode]): Unit = {
    val _node = new NodeTailCall(id, sig, callee, args)
    onNewNodeCreated(_node)
  }
  def newRet(id: MuID, rvs: Seq[MuVarNode]): Unit = {
    val _node = new NodeRet(id, rvs)
    onNewNodeCreated(_node)
  }
  def newThrow(id: MuID, exc: MuVarNode): Unit = {
    val _node = new NodeThrow(id, exc)
    onNewNodeCreated(_node)
  }
  def newExtractValue(id: MuID, resultID: MuID, strty: MuTypeNode, index: Int, opnd: MuVarNode): Unit = {
    val _node = new NodeExtractValue(id, resultID, strty, index, opnd)
    onNewNodeCreated(_node)
  }
  def newInsertValue(id: MuID, resultID: MuID, strty: MuTypeNode, index: Int, opnd: MuVarNode, newval: MuVarNode): Unit = {
    val _node = new NodeInsertValue(id, resultID, strty, index, opnd, newval)
    onNewNodeCreated(_node)
  }
  def newExtractElement(id: MuID, resultID: MuID, seqty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode): Unit = {
    val _node = new NodeExtractElement(id, resultID, seqty, indty, opnd, index)
    onNewNodeCreated(_node)
  }
  def newInsertElement(id: MuID, resultID: MuID, seqty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode, newval: MuVarNode): Unit = {
    val _node = new NodeInsertElement(id, resultID, seqty, indty, opnd, index, newval)
    onNewNodeCreated(_node)
  }
  def newShuffleVector(id: MuID, resultID: MuID, vecty: MuTypeNode, maskty: MuTypeNode, vec1: MuVarNode, vec2: MuVarNode, mask: MuVarNode): Unit = {
    val _node = new NodeShuffleVector(id, resultID, vecty, maskty, vec1, vec2, mask)
    onNewNodeCreated(_node)
  }
  def newNew(id: MuID, resultID: MuID, allocty: MuTypeNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeNew(id, resultID, allocty, excClause)
    onNewNodeCreated(_node)
  }
  def newNewHybrid(id: MuID, resultID: MuID, allocty: MuTypeNode, lenty: MuTypeNode, length: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeNewHybrid(id, resultID, allocty, lenty, length, excClause)
    onNewNodeCreated(_node)
  }
  def newAlloca(id: MuID, resultID: MuID, allocty: MuTypeNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeAlloca(id, resultID, allocty, excClause)
    onNewNodeCreated(_node)
  }
  def newAllocaHybrid(id: MuID, resultID: MuID, allocty: MuTypeNode, lenty: MuTypeNode, length: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeAllocaHybrid(id, resultID, allocty, lenty, length, excClause)
    onNewNodeCreated(_node)
  }
  def newGetIRef(id: MuID, resultID: MuID, refty: MuTypeNode, opnd: MuVarNode): Unit = {
    val _node = new NodeGetIRef(id, resultID, refty, opnd)
    onNewNodeCreated(_node)
  }
  def newGetFieldIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, index: Int, opnd: MuVarNode): Unit = {
    val _node = new NodeGetFieldIRef(id, resultID, isPtr, refty, index, opnd)
    onNewNodeCreated(_node)
  }
  def newGetElemIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, indty: MuTypeNode, opnd: MuVarNode, index: MuVarNode): Unit = {
    val _node = new NodeGetElemIRef(id, resultID, isPtr, refty, indty, opnd, index)
    onNewNodeCreated(_node)
  }
  def newShiftIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, offty: MuTypeNode, opnd: MuVarNode, offset: MuVarNode): Unit = {
    val _node = new NodeShiftIRef(id, resultID, isPtr, refty, offty, opnd, offset)
    onNewNodeCreated(_node)
  }
  def newGetVarPartIRef(id: MuID, resultID: MuID, isPtr: Boolean, refty: MuTypeNode, opnd: MuVarNode): Unit = {
    val _node = new NodeGetVarPartIRef(id, resultID, isPtr, refty, opnd)
    onNewNodeCreated(_node)
  }
  def newLoad(id: MuID, resultID: MuID, isPtr: Boolean, ord: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeLoad(id, resultID, isPtr, ord, refty, loc, excClause)
    onNewNodeCreated(_node)
  }
  def newStore(id: MuID, isPtr: Boolean, ord: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, newval: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeStore(id, isPtr, ord, refty, loc, newval, excClause)
    onNewNodeCreated(_node)
  }
  def newCmpXchg(id: MuID, valueResultID: MuID, succResultID: MuID, isPtr: Boolean, isWeak: Boolean, ordSucc: MemoryOrder.Value, ordFail: MemoryOrder.Value, refty: MuTypeNode, loc: MuVarNode, expected: MuVarNode, desired: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeCmpXchg(id, valueResultID, succResultID, isPtr, isWeak, ordSucc, ordFail, refty, loc, expected, desired, excClause)
    onNewNodeCreated(_node)
  }
  def newAtomicRMW(id: MuID, resultID: MuID, isPtr: Boolean, ord: MemoryOrder.Value, optr: AtomicRMWOptr.Value, refTy: MuTypeNode, loc: MuVarNode, opnd: MuVarNode, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeAtomicRMW(id, resultID, isPtr, ord, optr, refTy, loc, opnd, excClause)
    onNewNodeCreated(_node)
  }
  def newFence(id: MuID, ord: MemoryOrder.Value): Unit = {
    val _node = new NodeFence(id, ord)
    onNewNodeCreated(_node)
  }
  def newTrap(id: MuID, resultIDs: Seq[MuID], rettys: Seq[MuTypeNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeTrap(id, resultIDs, rettys, excClause, keepaliveClause)
    onNewNodeCreated(_node)
  }
  def newWatchPoint(id: MuID, wpid: MuWPID, resultIDs: Seq[MuID], rettys: Seq[MuTypeNode], dis: MuDestClause, ena: MuDestClause, exc: Option[MuDestClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeWatchPoint(id, wpid, resultIDs, rettys, dis, ena, exc, keepaliveClause)
    onNewNodeCreated(_node)
  }
  def newWPBranch(id: MuID, wpid: MuWPID, dis: MuDestClause, ena: MuDestClause): Unit = {
    val _node = new NodeWPBranch(id, wpid, dis, ena)
    onNewNodeCreated(_node)
  }
  def newCCall(id: MuID, resultIDs: Seq[MuID], callconv: Flag, calleeTy: MuTypeNode, sig: MuFuncSigNode, callee: MuVarNode, args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeCCall(id, resultIDs, callconv, calleeTy, sig, callee, args, excClause, keepaliveClause)
    onNewNodeCreated(_node)
  }
  def newNewThread(id: MuID, resultID: MuID, stack: MuVarNode, threadlocal: Option[MuVarNode], newStackClause: MuNewStackClause, excClause: Option[MuExcClause]): Unit = {
    val _node = new NodeNewThread(id, resultID, stack, threadlocal, newStackClause, excClause)
    onNewNodeCreated(_node)
  }
  def newSwapStack(id: MuID, resultIDs: Seq[MuID], swappee: MuVarNode, curStackClause: MuCurStackClause, newStackClause: MuNewStackClause, excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeSwapStack(id, resultIDs, swappee, curStackClause, newStackClause, excClause, keepaliveClause)
    onNewNodeCreated(_node)
  }
  def newCommInst(id: MuID, resultIDs: Seq[MuID], opcode: CommInst, flags: Seq[Flag], tys: Seq[MuTypeNode], sigs: Seq[MuFuncSigNode], args: Seq[MuVarNode], excClause: Option[MuExcClause], keepaliveClause: Option[MuKeepaliveClause]): Unit = {
    val _node = new NodeCommInst(id, resultIDs, opcode, flags, tys, sigs, args, excClause, keepaliveClause)
    onNewNodeCreated(_node)
  }
  // GEN:END:IRBUILDERNODE_CONSTRUCTORS

  // SCRIPT: END HERE
}
