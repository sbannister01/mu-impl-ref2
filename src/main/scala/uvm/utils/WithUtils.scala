package uvm.utils

/**
 * Emulate Java 1.7 try-with-resource in Scala. Unlike Java, res cannot be null.
 */
object WithUtils {
  def tryWithResource[T <: AutoCloseable, U](res: T)(f: T => U): U = {
    var maybePrimaryExc: Option[Throwable] = None
    try {
      f(res)
    } catch {
      case t: Throwable =>
        maybePrimaryExc = Some(t)
        throw t
    } finally {
      maybePrimaryExc match {
        case None => res.close()
        case Some(primaryExc) => try {
          res.close()
        } catch {
          case secondaryExc: Throwable =>
            primaryExc.addSuppressed(secondaryExc)
        }
      }
    }
  }
}