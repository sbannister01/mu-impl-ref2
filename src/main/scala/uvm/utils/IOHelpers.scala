package uvm.utils

import java.io.BufferedReader
import java.io.InputStream
import java.io.OutputStream
import java.io.Reader
import scala.collection.mutable.ArrayBuffer

object IOHelpers {
  def slurp(r: Reader): String = {
    val sb = new StringBuilder()
    val cb = new Array[Char](4096)

    var finished = false
    while (!finished) {
      val actualRead = r.read(cb, 0, 4096)
      if (actualRead > 0) {
        sb.appendAll(cb, 0, actualRead)
      } else {
        finished = true
      }
    }

    sb.toString()
  }

  private val BYTE_BUFFER_SIZE = 16384

  def copy(in: InputStream, out: OutputStream): Unit = {
    val buf = new Array[Byte](BYTE_BUFFER_SIZE)

    var finished = false
    while (!finished) {
      val actualRead = in.read(buf, 0, BYTE_BUFFER_SIZE)
      if (actualRead > 0) {
        out.write(buf, 0, actualRead)
      } else {
        finished = true
      }
    }
  }

  def forEachLine(br: BufferedReader)(f: String => Unit): Unit = {
    var finished = false
    while (!finished) {
      Option(br.readLine()) match {
        case None => finished = true
        case Some(line) => {
          f(line)
        }
      }
    }
  }

  def readLines(br: BufferedReader): Seq[String] = {
    val ab = ArrayBuffer[String]()
    forEachLine(br) { line =>
      ab += line
    }
    ab
  }
}