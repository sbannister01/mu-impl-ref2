package uvm.utils

import scala.collection.Map

import uvm.Identified

object IDFactory {
  def newClientIDFactory() = new SequentialIDFactory(Identified.FIRST_CLIENT_USABLE_ID)
  def newPrivateIDFactory() = new SequentialIDFactory(Identified.FIRST_PRIVATE_ID)
  def newOneBasedIDFactory() = new SequentialIDFactory(1)
}

abstract class IDFactory {
  def getID(): Int
  def getID(name: String): Int
  def setNextID(nextID: Int): Unit
}

class SequentialIDFactory(initialID: Int) extends IDFactory {
  private var id: Int = initialID

  def getID(): Int = {
    val myID = id
    id = id + 1
    return myID
  }

  def getID(name: String): Int = {
    val myID = id
    id = id + 1
    return myID
  }

  def setNextID(nextID: Int): Unit = {
    id = nextID
  }
}

class MappedIDFactory(nameToID: Map[String, Int]) extends IDFactory {
  def getID(): Int = {
    throw new UnsupportedOperationException("MappedIDFactory can only get ID from name")
  }

  def getID(name: String): Int = {
    return nameToID(name)
  }

  def setNextID(nextID: Int): Unit = {
    throw new UnsupportedOperationException("MappedIDFactory cannot set the next ID")
  }
}