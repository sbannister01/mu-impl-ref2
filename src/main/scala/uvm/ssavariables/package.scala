package uvm

package object ssavariables {
  type BinOpStatus = Int
  val BOS_N: BinOpStatus = 1
  val BOS_Z: BinOpStatus = 2
  val BOS_C: BinOpStatus = 4
  val BOS_V: BinOpStatus = 8
  
  object BinOpStatus {
    val ALL_FLAGS = Seq(BOS_N, BOS_Z, BOS_C, BOS_V)
    
    def numOfFlags(flags: BinOpStatus): Int = {
      Integer.bitCount(flags)
    }
  }
}