package uvm

import uvm.refimpl.nat.PlatformConstants
import uvm.refimpl.itpr.InterpreterThread
import uvm.refimpl.itpr.FrameCursor
import uvm.refimpl.itpr.InterpreterStack
import uvm.ir.irbuilder.IRBuilder

package object refimpl {
  /** Internal identifier of various things, such as threads and stacks, not related to MuID. */
  type MuInternalID = Int

  type Word = PlatformConstants.Word
  val WORD_SIZE_LOG = PlatformConstants.WORD_SIZE_LOG
  val WORD_SIZE_BITS = PlatformConstants.WORD_SIZE_BITS
  val WORD_SIZE_BYTES = PlatformConstants.WORD_SIZE_BYTES

  type MuFuncRefValue = MuOpaqueRefValue[Function]
  type MuThreadRefValue = MuOpaqueRefValue[InterpreterThread]
  type MuStackRefValue = MuOpaqueRefValue[InterpreterStack]
  type MuFCRefValue = MuOpaqueRefValue[FrameCursor]
  type MuIRBuilderRefValue = MuOpaqueRefValue[IRBuilder]

  def MuFuncRefValue = MuOpaqueRefValue.apply[Function] _
  def MuThreadRefValue = MuOpaqueRefValue.apply[InterpreterThread] _
  def MuStackRefValue = MuOpaqueRefValue.apply[InterpreterStack] _
  def MuFCRefValue = MuOpaqueRefValue.apply[FrameCursor] _
  def MuIRBuilderRefValue = MuOpaqueRefValue.apply[IRBuilder] _
}