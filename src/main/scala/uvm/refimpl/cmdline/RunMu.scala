package uvm.refimpl.cmdline

import scala.collection.mutable.HashMap

import uvm.refimpl.MicroVM
import uvm.refimpl.VMConf
import uvm.utils.WithUtils.tryWithResource

/** Run Mu from the command line. */
object RunMu {
  private val KVOPT = """--(\w+)=(.+)""".r
  private val SEP = "--"
  private val HELP = "-h|--help".r

  private val ENTRYPOINT_FILE = "entrypoint"

  def main(args: Array[String]): Unit = {
    val props = new HashMap[String, String]()

    var curArgs = args.toList
    var parsing = true

    while (parsing) {
      curArgs match {
        case Nil => parsing = false
        case arg :: rest => {
          arg match {
            case HELP() => {
              System.err.println(DOCSTRING)
              return
            }
            case SEP => {
              curArgs = rest
              parsing = false
            }
            case KVOPT(k, v) => {
              props(k) = v
              curArgs = rest
            }
            case _ => {
              parsing = false
            }
          }
        }
      }

    }

    val (bootImg, appArgs) = curArgs match {
      case h :: t => (h, t)
      case Nil => {
        System.err.println(DOCSTRING)
        return
      }
    }

    props("bootImg") = bootImg
    
    val allArgs = bootImg :: appArgs

    tryWithResource(MicroVM(VMConf(props.toMap), allArgs)) { microVM =>
      microVM.execute()
    }
  }

  private val DOCSTRING = """USAGE: runmu.sh --prop1=value1 --prop2=value2 ... [--] bootimg [args...]

Properties and values are the same as the argument of mu_refimpl_new_ex. See
README.md for more information.

bootimg is the path to the boot image (overrides the bootImg property if set).
It will start execution at the entry point specified in the boot image.
Extra args will be passed to the entry point function.
"""
}