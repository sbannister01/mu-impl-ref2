package uvm.refimpl.bootimg

import uvm.types.Type
import uvm.refimpl.Word

object BootImageFile {
  val FILEEXT_DATA = ".data"
  val FILEEXT_ALLOC = ".alloc"
  val FILEEXT_RELOC = ".reloc"

  val UIRBUNDLE_FILE = "bundle.uir"
  val IDNAMEMAP_FILE = "idnamemap"
  val MANUALSYMS_FILE = "manualsyms"
  val METAINFO_FILE = "metainfo"
  
  val KEY_INITFUNC = "initfunc"
  val KEY_THREADLOCAL = "threadlocal"
  val KEY_EXTRALIBS = "extralibs"

  val AS_REF = "R"
  val AS_IREF = "I"
  val AS_TAGREF = "T"
  val AS_FUNCREF = "F"
  val AS_UPTR = "P"

  val TO_GLOBAL = "G"
  val TO_HEAP = "H"
  val TO_FUNC = "F"
  val TO_SYM = "S"

  val NO_OFFSET = 0L
  val NO_SYM = ""

  /**
   * Allocation record. For global and heap.
   *
   * @param num An integer to identify the unit in a .data file. For global, it is the ID of the global cell; for heap
   * object, it is a sequential number.
   * @param fileOffset The offset from the beginning of the .data file.
   * @param ty The type of the allocation unit.
   * @param varLen The length of the variable part if it is a hybrid; otherwise 0.
   *
   * @param addr The address in the current micro VM instance. Not persisted.
   */
  case class UnitAllocRecord(num: Long, fileOffset: Long, ty: Type, varLen: Long, addr: Word)

  /**
   * Manual symbols. For global only.  The heap cannot have any manual symbols, because the pinning of primordial
   * objects is not supported.
   *
   * @param name The symbol name.  Used for symbol lookup.
   * @param targetNum The ID of the global cell.
   * @param targetOffset Offset within the global cell.
   */
  case class ManualSymRecord(sym: String, targetNum: Long, targetOffset: Word)

  /**
   * Relocation record. One for each field of ref<T>, iref<T>, weakref<T> or funcref<sig> types.
   *
   * @param num The source allocation unit number.
   * @param fieldOffset The offset of the field from the beginning of the allocation unit.
   * @param targetKind Whether the target is in the global memory, heap, or is a function.
   * @param targetNum To identify the target. For memory, it is the beginning of the allocation unit; for function, it
   * is the ID of the function.
   * @param targetOffset If the source is an iref<T>, it is the offset from the beginning of the target allocation unit;
   * otherwise 0.
   */
  case class FieldRelocRecord(num: Long, fieldOffset: Long, fieldKind: String, targetKind: String, targetNum: Long, targetOffset: Long, targetString: String)
}