package uvm.refimpl.mem

import scala.collection.mutable.HashMap

/**
 * This class keeps a map between Long and arbitrary Scala objects. In this way,
 * references to arbitrary Scala objects can be stored in the Mu memory.
 * <p>
 * Similar to JFFI's ObjectReferenceManager.
 */
class SequentialObjectKeeper[T] {
  private val longToObjMap = new HashMap[Long, T]()
  private val objToLongMap = new HashMap[T, Long]()

  var nextLong = 1L

  def objGetLong(obj: T): Long = {
    objToLongMap.getOrElseUpdate(obj, {
      val myLong = nextLong
      nextLong += 1
      longToObjMap(myLong) = obj
      myLong
    })
  }

  def longToObj(l: Long): T = longToObjMap.apply(l)

  def maybeRemoveObj(obj: T): Unit = {
    objToLongMap.remove(obj).foreach { l =>
      longToObjMap.remove(l)
    }
  }
}