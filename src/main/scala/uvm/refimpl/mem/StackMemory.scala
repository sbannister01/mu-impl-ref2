package uvm.refimpl.mem

import uvm.refimpl.{MicroVM, Word}
import uvm.refimpl.mem.bumppointer.RewindableBumpPointerAllocator

/**
 * Stack memory.
 * <p>
 * If the stack is Dead, the stackObjRef will be a dangling pointer.
 */
class StackMemory(val stackObjRef: Word, extend: Word)(
    implicit microVM: MicroVM, memorySupport: MemorySupport)
    extends RewindableBumpPointerAllocator(stackObjRef, extend)
