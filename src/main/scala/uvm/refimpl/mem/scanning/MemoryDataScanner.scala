package uvm.refimpl.mem.scanning

import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger
import com.typesafe.scalalogging.StrictLogging

import uvm.Function
import uvm.ir.irbuilder.IRBuilder
import uvm.refimpl._
import uvm.refimpl.itpr._
import uvm.refimpl.mem._
import uvm.refimpl.mem.TypeSizes._
import uvm.types._

object MemoryDataScanner extends StrictLogging {

  val paranoiaLogger = Logger(LoggerFactory.getLogger(getClass.getName() + ".paranoia"))

  /**
   * Scan an allocation unit. In this implementation, heap objects, alloca cells and global
   * cells all have the same layout.
   */
  def scanAllocUnit(objRef: Word, handler: MemoryFieldHandler)(implicit microVM: MicroVM, memorySupport: MemorySupport) {
    val tag = HeaderUtils.getTag(objRef)
    logger.debug("Obj 0x%x, tag 0x%x".format(objRef, tag))
    val ty = HeaderUtils.getType(microVM, tag)
    logger.debug {
      if (ty.isInstanceOf[TypeHybrid]) {
        val len = HeaderUtils.getVarLength(objRef)
        val size = TypeSizes.hybridSizeOf(ty.asInstanceOf[TypeHybrid], len)
        "Type: %s, varLen: %d, hybridSizeOf: %d".format(ty.repr, len, size)
      } else {
        "Type: %s".format(ty.repr)
      }
    }
    scanField(ty, objRef, objRef, handler)
  }

  def scanField(ty: Type, objRef: Word, iRef: Word, handler: MemoryFieldHandler)(implicit microVM: MicroVM, memorySupport: MemorySupport) {
    def logField(kind: String, toObj: Word): String = "%s field [0x%x + 0x%x] = 0x%x -> 0x%x".format(kind, objRef, iRef - objRef, iRef, toObj)
    def logIRefField(kind: String, toObj: Word, offset: Word): String = "%s field [0x%x + 0x%x] = 0x%x -> 0x%x + 0x%x = 0x%x".format(
        kind, objRef, iRef - objRef, iRef, toObj, offset, toObj + offset)
    ty match {
      case t: TypeUPtr => {
        val toAddr = memorySupport.loadLong(iRef, false)
        logger.debug(logField("UPtr", toAddr))
        handler.visitUPtrField(objRef, iRef, toAddr)
      }
      case t: TypeUFuncPtr => {
        val toAddr = memorySupport.loadLong(iRef, false)
        logger.debug(logField("UFuncPtr", toAddr))
        handler.visitUFPField(objRef, iRef, toAddr)
      }
      case t: TypeRef => {
        val toObj = memorySupport.loadLong(iRef)
        logger.debug(logField("Ref", toObj))
        handler.visitRefField(objRef, iRef, toObj, false)
      }
      case t: TypeIRef => {
        val toObj = memorySupport.loadLong(iRef)
        val offset = memorySupport.loadLong(iRef + WORD_SIZE_BYTES)
        logger.debug(logIRefField("IRef", toObj, offset))
        handler.visitIRefField(objRef, iRef, toObj, offset)
      }
      case t: TypeWeakRef => {
        val toObj = memorySupport.loadLong(iRef)
        logger.debug(logField("WeakRef", toObj))
        handler.visitRefField(objRef, iRef, toObj, true)
      }
      case t: TypeTagRef64 => {
        val bits = memorySupport.loadLong(iRef)
        if (paranoiaLogger.underlying.isDebugEnabled()) {
          paranoiaLogger.debug(s"Tagref bits ${bits}")
          if (OpHelper.tr64IsFP(bits)) {
            paranoiaLogger.debug("Tagref is FP: %f".format(OpHelper.tr64ToFP(bits)))
          } else if (OpHelper.tr64IsInt(bits)) {
            paranoiaLogger.debug("Tagref is Int: %d".format(OpHelper.tr64ToInt(bits)))
          } else if (OpHelper.tr64IsRef(bits)) {
            paranoiaLogger.debug("Tagref is Ref: 0x%x tag: %d".format(OpHelper.tr64ToRef(bits), OpHelper.tr64ToTag(bits)))
          }
        }
        if (OpHelper.tr64IsRef(bits)) {
          val toObj = OpHelper.tr64ToRef(bits)
          logger.debug(logField("TagRef64", toObj))
          handler.visitTagRefField(objRef, iRef, toObj)
        }
      }
      case t: TypeStruct => {
        var fieldAddr = iRef
        for (fieldTy <- t.fieldTys) {
          val fieldAlign = TypeSizes.alignOf(fieldTy)
          fieldAddr = TypeSizes.alignUp(fieldAddr, fieldAlign)
          scanField(fieldTy, objRef, fieldAddr, handler)
          fieldAddr += TypeSizes.sizeOf(fieldTy)
        }
      }
      case t: TypeArray => {
        val elemTy = t.elemTy
        val elemSize = TypeSizes.sizeOf(elemTy)
        val elemAlign = TypeSizes.alignOf(elemTy)
        var elemAddr = iRef
        for (i <- 0L until t.len) {
          scanField(elemTy, objRef, elemAddr, handler)
          elemAddr = TypeSizes.alignUp(elemAddr + elemSize, elemAlign)
        }

      }
      case t: TypeHybrid => {
        val varTy = t.varTy
        val varSize = TypeSizes.sizeOf(varTy)
        val varAlign = TypeSizes.alignOf(varTy)
        val varLength = HeaderUtils.getVarLength(iRef)
        var curAddr = iRef
        for (fieldTy <- t.fieldTys) {
          val fieldAlign = TypeSizes.alignOf(fieldTy)
          curAddr = TypeSizes.alignUp(curAddr, fieldAlign)
          scanField(fieldTy, objRef, curAddr, handler)
          curAddr += TypeSizes.sizeOf(fieldTy)
        }
        curAddr = TypeSizes.alignUp(curAddr, varAlign)
        for (i <- 0L until varLength) {
          scanField(varTy, objRef, curAddr, handler)
          curAddr = TypeSizes.alignUp(curAddr + varSize, varAlign)
        }
      }
      case t: TypeStackRef => {
        val obj = memorySupport.loadEntity[InterpreterStack](iRef)
        handler.visitStackRefField(objRef, iRef, obj)
      }
      case t: TypeFuncRef => {
        val obj = memorySupport.loadEntity[Function](iRef)
        handler.visitFuncRefField(objRef, iRef, obj)
      }
      case t: TypeThreadRef => {
        val obj = memorySupport.loadEntity[InterpreterThread](iRef)
        handler.visitThreadRefField(objRef, iRef, obj)
      }
      case t: TypeFrameCursorRef => {
        val obj = memorySupport.loadEntity[FrameCursor](iRef)
        handler.visitFCRefField(objRef, iRef, obj)
      }
      case t: TypeIRBuilderRef => {
        val obj = memorySupport.loadEntity[IRBuilder](iRef)
        handler.visitIRBuilderRefField(objRef, iRef, obj)
      }
      case _ => // Ignore non-reference fields.
    }
  }
}
