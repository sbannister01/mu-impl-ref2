package uvm.refimpl.nat

import scala.collection.mutable.ArrayBuffer

import com.kenai.jffi.ForeignThief

import uvm.refimpl.UvmExternalLibraryException

object NativeLibraryHolder {
  /**
   * Create a NativeLibraryHolder, load the default library, and additional libraries.
   */
  def apply(libs: String*): NativeLibraryHolder = {
    val nlh = new NativeLibraryHolder()

    nlh.loadDefaultLibrary()

    for (lib <- libs) {
      nlh.loadLibrary(lib)
    }

    nlh
  }
}

class ManualLibrary(val handle: Long) extends AutoCloseable {
  
  def maybeGetSymbolAddress(symbol: String): Option[Long] = {
    try {
      Some(getSymbolAddress(symbol))
    } catch {
      case e: UnsatisfiedLinkError => None
    }
  }

  @throws[UnsatisfiedLinkError]("if symbol is not found in the library")
  def getSymbolAddress(symbol: String): Long = {
    ForeignThief.dlsym(handle, symbol)
  }

  override def close(): Unit = {
    if (handle != 0L) {
      ForeignThief.dlclose(handle)
    }
  }
}

/**
 * A holder of jffi Library instances. It can find symbols from all loaded libraries.
 */
class NativeLibraryHolder() extends AutoCloseable {
  val libs = new ArrayBuffer[ManualLibrary]()

  override def close(): Unit = {
    for (lib <- libs) {
      lib.close()
    }
  }

  /**
   * Load the default library: the current process.
   */
  def loadDefaultLibrary(): ManualLibrary = {
    val handle = ForeignThief.dlopen(null, ForeignThief.NOW)
    val lib = new ManualLibrary(handle)
    libs += lib
    lib
  }

  /**
   * Load a specified library.
   *
   * @param path the path. eSee the man page of dlopen for the path format.
   */
  def loadLibrary(path: String): ManualLibrary = {
    val maybeHandle = Option(ForeignThief.dlopen(path, ForeignThief.NOW))
    maybeHandle match {
      case None => throw new UvmExternalLibraryException("Cannot load library: %s".format(path))
      case Some(handle) =>
        val lib = new ManualLibrary(handle)
        libs += lib
        lib
    }
  }
  
  /**
   * Get the address of a symbol.
   *
   * @param symbol the symbol.
   * @return Some(addr) if the address is found, or None otherwise
   */
  def maybeGetSymbolAddress(symbol: String): Option[Long] = {
    for (lib <- libs) {
      lib.maybeGetSymbolAddress(symbol) match {
        case None =>
        case Some(addr) => return Some(addr)
      }
    }
    None
  }

  /**
   * Get the address of a symbol.
   *
   * @param symbol the symbol.
   * @return The address of the symbol
   * @throws UvmExternalLibraryException if the symbol cannot be resolved
   */
  @throws[UvmExternalLibraryException]("symbol cannot be resolved")
  def getSymbolAddress(symbol: String): Long = {
    maybeGetSymbolAddress(symbol) match {
      case None => throw new UvmExternalLibraryException("Symbol cannot be resolved: %s".format(symbol))
      case Some(addr) => addr
    }
  }
}
