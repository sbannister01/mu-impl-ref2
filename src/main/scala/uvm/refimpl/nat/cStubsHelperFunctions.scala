package uvm.refimpl.nat

import com.kenai.jffi.CallingConvention
import com.kenai.jffi.Closure
import com.kenai.jffi.Closure.Buffer
import com.kenai.jffi.{ Type => JType }

import CDefsHelperFunctions._
import NativeSupport._
import uvm.comminsts.CommInst
import uvm.comminsts.CommInsts
import uvm.ir.irbuilder.IRBuilder
import uvm.refimpl._
import uvm.refimpl.MicroVM
import uvm.ssavariables.{ AtomicRMWOptr, MemoryOrder }
import uvm.ssavariables.BinOpStatus
import uvm.ssavariables.Flag

class ExposedMethod(name: String, jRetTy: JType, jParamTys: Array[JType], invokeFunc: Buffer => Unit) {
  val closure = new SimpleClosure(name, invokeFunc)
  val handle = jffiClosureManager.newClosure(closure, jRetTy, jParamTys, CallingConvention.DEFAULT)
  def address = handle.getAddress()
}

class SimpleClosure(name: String, f: Buffer => Unit) extends Closure {
  def invoke(buffer: Buffer): Unit = try {
    f(buffer)
  } catch {
    case t: Throwable => {
      MuErrorNumber.setMuError(MuErrorNumber.MU_NATIVE_ERRNO)
      NativeClientSupport.logger.error(
          "Error thrown in Mu while native is calling %s. This is fatal.".format(name), t)
    }
  }
}

private object CDefsHelperFunctions {
  import NativeClientSupport._
  import NativeMemoryAccessHelper._

  def exposedMethod(name: String, jRetTy: JType, jParamTys: Array[JType])(invokeFunc: Buffer => Unit) = {
    new ExposedMethod(name: String, jRetTy, jParamTys, invokeFunc)
  }
  
  def readMuIDOptional(id: MuID): Option[MuID] = {
    if (id == 0) None else Some(id)
  }
  
  def readMuIDArray(base: Long, len: Long): IndexedSeq[MuID] = readIntArray(base, len)

  def readMuValueArray(base: Long, len: Long): IndexedSeq[MuValue] = {
    readLongArray(base, len).map(getMuValueNotNull)
  }

  def readFlagArray(base: Long, len: Long): IndexedSeq[Flag] = {
    readIntArray(base, len).map(toCallConv)
  }
  
  def intToBoolean(v: Int): Boolean = v != 0
  def booleanToInt(v: Boolean): Int = if (v) 1 else 0
  
  def toMuFlag(cc: Int): MuFlag = cc
  def toCallConv(cc: Int): Flag = cc match {
    case 0x00 => Flag("#DEFAULT")
    case _    => throw new IllegalArgumentException("Unknown calling convention %d (0x%x)".format(cc, cc))
  }
  
  def toCommInst(ci: Int): CommInst = CommInsts(ci)

  
  def toBinOpStatus(s: MuBinOpStatus): BinOpStatus = s

  implicit class RichMuVM(val mvm: MicroVM) extends AnyVal {
    def setTrapHandler(trap_handler: MuTrapHandlerFP, userdata: MuCPtr): Unit = {
      mvm.setTrapHandler(new NativeTrapHandler(trap_handler, userdata))
    }

    def getMuErrorPtr(): MuCPtr = MuErrorNumber.muErrorPtr.address()
  }

  implicit class RichMuCtx(val ctx: MuCtx) extends AnyVal {
    def handleFromSInt8(num: Byte, len: Int): MuIntValue = ctx.handleFromInt(num, len)
    def handleFromUInt8(num: Byte, len: Int): MuIntValue = ctx.handleFromInt(BigInt(num) & 0xff, len)
    def handleFromSInt16(num: Short, len: Int): MuIntValue = ctx.handleFromInt(num, len)
    def handleFromUInt16(num: Short, len: Int): MuIntValue = ctx.handleFromInt(BigInt(num) & 0xffff, len)
    def handleFromSInt32(num: Int, len: Int): MuIntValue = ctx.handleFromInt(num, len)
    def handleFromUInt32(num: Int, len: Int): MuIntValue = ctx.handleFromInt(BigInt(num) & 0xffffffff, len)
    def handleFromSInt64(num: Long, len: Int): MuIntValue = ctx.handleFromInt(num, len)
    def handleFromUInt64(num: Long, len: Int): MuIntValue = ctx.handleFromInt(BigInt(num) & 0xffffffffffffffffL, len)
    def handleFromUInt64s(nums: IndexedSeq[Long], len: Int): MuIntValue = {
      val bigNum = unsignedLongSeqToBigInt(nums)
      ctx.handleFromInt(bigNum, len)
    }
    def handleToSInt8(h: MuIntValue): Byte = ctx.handleToSInt(h).toByte
    def handleToUInt8(h: MuIntValue): Byte = ctx.handleToUInt(h).toByte
    def handleToSInt16(h: MuIntValue): Short = ctx.handleToSInt(h).toShort
    def handleToUInt16(h: MuIntValue): Short = ctx.handleToUInt(h).toShort
    def handleToSInt32(h: MuIntValue): Int = ctx.handleToSInt(h).toInt
    def handleToUInt32(h: MuIntValue): Int = ctx.handleToUInt(h).toInt
    def handleToSInt64(h: MuIntValue): Long = ctx.handleToSInt(h).toLong
    def handleToUInt64(h: MuIntValue): Long = ctx.handleToUInt(h).toLong

    def cmpxchg(ordSucc: MemoryOrder.Value, ordFail: MemoryOrder.Value, weak: Boolean,
      loc: MuIRefValue, expected: MuValue, desired: MuValue, is_succ: IntPtr): MuValue = {
      val (v, s) = ctx.cmpXchg(ordSucc, ordFail, weak, loc, expected, desired)
      val sInt = if (s) 1 else 0
      theMemory.putInt(is_succ, sInt)
      v
    }

    def atomicrmw(ord: MemoryOrder.Value, op: AtomicRMWOptr.Value, loc: MuIRefValue, opnd: MuValue): MuValue = {
      ctx.atomicRMW(ord, op, loc, opnd)
    }

    def newThreadNor(stack: MuStackRefValue, threadLocal: Option[MuRefValue], vals: Seq[MuValue]): MuThreadRefValue = {
      ctx.newThread(stack, threadLocal, HowToResume.PassValues(vals))
    }

    def newThreadExc(stack: MuStackRefValue, threadLocal: Option[MuRefValue], exc: MuRefValue): MuThreadRefValue = {
      ctx.newThread(stack, threadLocal, HowToResume.ThrowExc(exc))
    }

    def dumpKeepalives(cursor: MuFCRefValue, results: MuValueFakArrayPtr): Unit = {
      val kas = ctx.dumpKeepalives(cursor)
      for ((ka, i) <- kas.zipWithIndex) {
        val fak = exposeMuValue(ctx, ka)
        val addr = results + i * WORD_SIZE_BYTES
        theMemory.putAddress(addr, fak)
      }
    }
  }
  
  implicit class RichMuIRBuilder(val b: IRBuilder) extends AnyVal {
    def newConstIntEx(id: MuID, ty: MuID, values: Seq[Long]): Unit = {
      val bigNum = unsignedLongSeqToBigInt(values)
      b.newConstInt(id, ty, bigNum)
    }
  }
  
  implicit def makeMuValueSeqCovariant[T <: MuValue, U <: T](seq: Seq[T]): Seq[U] = seq.asInstanceOf[Seq[U]]
  implicit def muValueAutoCast[T <: MuValue, U <: T](v: T): U = v.asInstanceOf[U]

}