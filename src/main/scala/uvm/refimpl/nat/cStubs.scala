package uvm.refimpl.nat

import com.kenai.jffi.{ Type => JType }

import NativeSupport._
import uvm.refimpl._
import uvm.ssavariables.{BinOptr, CmpOptr, ConvOptr, MemoryOrder, AtomicRMWOptr}

object CDefs {
  import NativeMemoryAccessHelper._
  import CDefsHelperFunctions._
  import NativeClientSupport._

  // generated from migrate_scripts/muapitocstubs.py
  /// GEN:BEGIN:STUBS
val MUVM__NEW_CONTEXT = exposedMethod("MuVM.new_context", JType.POINTER, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val mvm = getMicroVM(_raw_mvm)
val _RV = mvm.newContext()
val _RV_FAK = exposeMuCtx(_RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUVM__ID_OF = exposedMethod("MuVM.id_of", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val _raw_name = _jffiBuffer.getAddress(1)
val mvm = getMicroVM(_raw_mvm)
val name = readCString(_raw_name)
val _RV = mvm.idOf(name)
_jffiBuffer.setIntReturn(_RV)
}
val MUVM__NAME_OF = exposedMethod("MuVM.name_of", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val mvm = getMicroVM(_raw_mvm)
val id = _raw_id
val _RV = mvm.nameOf(id)
val _RV_FAK = exposeString(_RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUVM__SET_TRAP_HANDLER = exposedMethod("MuVM.set_trap_handler", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val _raw_trap_handler = _jffiBuffer.getAddress(1)
val _raw_userdata = _jffiBuffer.getAddress(2)
val mvm = getMicroVM(_raw_mvm)
val trap_handler = _raw_trap_handler
val userdata = _raw_userdata
val _RV = mvm.setTrapHandler(trap_handler, userdata)
}
val MUVM__EXECUTE = exposedMethod("MuVM.execute", JType.VOID, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val mvm = getMicroVM(_raw_mvm)
val _RV = mvm.execute()
}
val MUVM__GET_MU_ERROR_PTR = exposedMethod("MuVM.get_mu_error_ptr", JType.POINTER, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_mvm = _jffiBuffer.getAddress(0)
val mvm = getMicroVM(_raw_mvm)
val _RV = mvm.getMuErrorPtr()
_jffiBuffer.setAddressReturn(_RV)
}
val stubsOfMuVM = new Array[Word](6)
stubsOfMuVM(0) = MUVM__NEW_CONTEXT.address
stubsOfMuVM(1) = MUVM__ID_OF.address
stubsOfMuVM(2) = MUVM__NAME_OF.address
stubsOfMuVM(3) = MUVM__SET_TRAP_HANDLER.address
stubsOfMuVM(4) = MUVM__EXECUTE.address
stubsOfMuVM(5) = MUVM__GET_MU_ERROR_PTR.address
val MUCTX__ID_OF = exposedMethod("MuCtx.id_of", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_name = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val name = readCString(_raw_name)
val _RV = ctx.idOf(name)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__NAME_OF = exposedMethod("MuCtx.name_of", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val id = _raw_id
val _RV = ctx.nameOf(id)
val _RV_FAK = exposeString(_RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__CLOSE_CONTEXT = exposedMethod("MuCtx.close_context", JType.VOID, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val ctx = getMuCtx(_raw_ctx)
val _RV = ctx.closeContext()
}
val MUCTX__LOAD_BUNDLE = exposedMethod("MuCtx.load_bundle", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_buf = _jffiBuffer.getAddress(1)
val _raw_sz = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val buf = readCharArray(_raw_buf, _raw_sz)
val _RV = ctx.loadBundle(buf)
}
val MUCTX__LOAD_HAIL = exposedMethod("MuCtx.load_hail", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_buf = _jffiBuffer.getAddress(1)
val _raw_sz = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val buf = readCharArray(_raw_buf, _raw_sz)
val _RV = ctx.loadHail(buf)
}
val MUCTX__HANDLE_FROM_SINT8 = exposedMethod("MuCtx.handle_from_sint8", JType.POINTER, Array(JType.POINTER, JType.SINT8, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getByte(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromSInt8(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_UINT8 = exposedMethod("MuCtx.handle_from_uint8", JType.POINTER, Array(JType.POINTER, JType.UINT8, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getByte(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromUInt8(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_SINT16 = exposedMethod("MuCtx.handle_from_sint16", JType.POINTER, Array(JType.POINTER, JType.SINT16, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getShort(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromSInt16(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_UINT16 = exposedMethod("MuCtx.handle_from_uint16", JType.POINTER, Array(JType.POINTER, JType.UINT16, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getShort(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromUInt16(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_SINT32 = exposedMethod("MuCtx.handle_from_sint32", JType.POINTER, Array(JType.POINTER, JType.SINT32, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getInt(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromSInt32(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_UINT32 = exposedMethod("MuCtx.handle_from_uint32", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getInt(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromUInt32(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_SINT64 = exposedMethod("MuCtx.handle_from_sint64", JType.POINTER, Array(JType.POINTER, JType.SINT64, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getLong(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromSInt64(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_UINT64 = exposedMethod("MuCtx.handle_from_uint64", JType.POINTER, Array(JType.POINTER, JType.UINT64, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getLong(1)
val _raw_len = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val len = _raw_len
val _RV = ctx.handleFromUInt64(num, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_UINT64S = exposedMethod("MuCtx.handle_from_uint64s", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_nums = _jffiBuffer.getAddress(1)
val _raw_nnums = _jffiBuffer.getAddress(2)
val _raw_len = _jffiBuffer.getInt(3)
val ctx = getMuCtx(_raw_ctx)
val nums = readLongArray(_raw_nums, _raw_nnums)
val len = _raw_len
val _RV = ctx.handleFromUInt64s(nums, len)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_FLOAT = exposedMethod("MuCtx.handle_from_float", JType.POINTER, Array(JType.POINTER, JType.FLOAT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getFloat(1)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val _RV = ctx.handleFromFloat(num)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_DOUBLE = exposedMethod("MuCtx.handle_from_double", JType.POINTER, Array(JType.POINTER, JType.DOUBLE)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_num = _jffiBuffer.getDouble(1)
val ctx = getMuCtx(_raw_ctx)
val num = _raw_num
val _RV = ctx.handleFromDouble(num)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_PTR = exposedMethod("MuCtx.handle_from_ptr", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_mu_type = _jffiBuffer.getInt(1)
val _raw_ptr = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val mu_type = _raw_mu_type
val ptr = _raw_ptr
val _RV = ctx.handleFromPtr(mu_type, ptr)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_FP = exposedMethod("MuCtx.handle_from_fp", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_mu_type = _jffiBuffer.getInt(1)
val _raw_fp = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val mu_type = _raw_mu_type
val fp = _raw_fp
val _RV = ctx.handleFromFP(mu_type, fp)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_TO_SINT8 = exposedMethod("MuCtx.handle_to_sint8", JType.SINT8, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToSInt8(opnd)
_jffiBuffer.setByteReturn(_RV)
}
val MUCTX__HANDLE_TO_UINT8 = exposedMethod("MuCtx.handle_to_uint8", JType.UINT8, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToUInt8(opnd)
_jffiBuffer.setByteReturn(_RV)
}
val MUCTX__HANDLE_TO_SINT16 = exposedMethod("MuCtx.handle_to_sint16", JType.SINT16, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToSInt16(opnd)
_jffiBuffer.setShortReturn(_RV)
}
val MUCTX__HANDLE_TO_UINT16 = exposedMethod("MuCtx.handle_to_uint16", JType.UINT16, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToUInt16(opnd)
_jffiBuffer.setShortReturn(_RV)
}
val MUCTX__HANDLE_TO_SINT32 = exposedMethod("MuCtx.handle_to_sint32", JType.SINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToSInt32(opnd)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__HANDLE_TO_UINT32 = exposedMethod("MuCtx.handle_to_uint32", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToUInt32(opnd)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__HANDLE_TO_SINT64 = exposedMethod("MuCtx.handle_to_sint64", JType.SINT64, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToSInt64(opnd)
_jffiBuffer.setLongReturn(_RV)
}
val MUCTX__HANDLE_TO_UINT64 = exposedMethod("MuCtx.handle_to_uint64", JType.UINT64, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIntValue]
val _RV = ctx.handleToUInt64(opnd)
_jffiBuffer.setLongReturn(_RV)
}
val MUCTX__HANDLE_TO_FLOAT = exposedMethod("MuCtx.handle_to_float", JType.FLOAT, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuFloatValue]
val _RV = ctx.handleToFloat(opnd)
_jffiBuffer.setFloatReturn(_RV)
}
val MUCTX__HANDLE_TO_DOUBLE = exposedMethod("MuCtx.handle_to_double", JType.DOUBLE, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuDoubleValue]
val _RV = ctx.handleToDouble(opnd)
_jffiBuffer.setDoubleReturn(_RV)
}
val MUCTX__HANDLE_TO_PTR = exposedMethod("MuCtx.handle_to_ptr", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuUPtrValue]
val _RV = ctx.handleToPtr(opnd)
_jffiBuffer.setAddressReturn(_RV)
}
val MUCTX__HANDLE_TO_FP = exposedMethod("MuCtx.handle_to_fp", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuUFPValue]
val _RV = ctx.handleToFP(opnd)
_jffiBuffer.setAddressReturn(_RV)
}
val MUCTX__HANDLE_FROM_CONST = exposedMethod("MuCtx.handle_from_const", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val id = _raw_id
val _RV = ctx.handleFromConst(id)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_GLOBAL = exposedMethod("MuCtx.handle_from_global", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val id = _raw_id
val _RV = ctx.handleFromGlobal(id)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_FUNC = exposedMethod("MuCtx.handle_from_func", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val id = _raw_id
val _RV = ctx.handleFromFunc(id)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__HANDLE_FROM_EXPOSE = exposedMethod("MuCtx.handle_from_expose", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val id = _raw_id
val _RV = ctx.handleFromExpose(id)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__DELETE_VALUE = exposedMethod("MuCtx.delete_value", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuValue]
val _RV = ctx.deleteValue(opnd)
}
val MUCTX__REF_EQ = exposedMethod("MuCtx.ref_eq", JType.SINT, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_lhs = _jffiBuffer.getAddress(1)
val _raw_rhs = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val lhs = getMuValueNotNull(_raw_lhs).asInstanceOf[MuGenRefValue]
val rhs = getMuValueNotNull(_raw_rhs).asInstanceOf[MuGenRefValue]
val _RV = ctx.refEq(lhs, rhs)
val _RV_FAK = booleanToInt(_RV)
_jffiBuffer.setIntReturn(_RV_FAK)
}
val MUCTX__REF_ULT = exposedMethod("MuCtx.ref_ult", JType.SINT, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_lhs = _jffiBuffer.getAddress(1)
val _raw_rhs = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val lhs = getMuValueNotNull(_raw_lhs).asInstanceOf[MuIRefValue]
val rhs = getMuValueNotNull(_raw_rhs).asInstanceOf[MuIRefValue]
val _RV = ctx.refUlt(lhs, rhs)
val _RV_FAK = booleanToInt(_RV)
_jffiBuffer.setIntReturn(_RV_FAK)
}
val MUCTX__EXTRACT_VALUE = exposedMethod("MuCtx.extract_value", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_str = _jffiBuffer.getAddress(1)
val _raw_index = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val str = getMuValueNotNull(_raw_str).asInstanceOf[MuStructValue]
val index = _raw_index
val _RV = ctx.extractValue(str, index)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__INSERT_VALUE = exposedMethod("MuCtx.insert_value", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.SINT, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_str = _jffiBuffer.getAddress(1)
val _raw_index = _jffiBuffer.getInt(2)
val _raw_newval = _jffiBuffer.getAddress(3)
val ctx = getMuCtx(_raw_ctx)
val str = getMuValueNotNull(_raw_str).asInstanceOf[MuStructValue]
val index = _raw_index
val newval = getMuValueNotNull(_raw_newval).asInstanceOf[MuValue]
val _RV = ctx.insertValue(str, index, newval)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__EXTRACT_ELEMENT = exposedMethod("MuCtx.extract_element", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_str = _jffiBuffer.getAddress(1)
val _raw_index = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val str = getMuValueNotNull(_raw_str).asInstanceOf[MuSeqValue]
val index = getMuValueNotNull(_raw_index).asInstanceOf[MuIntValue]
val _RV = ctx.extractElement(str, index)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__INSERT_ELEMENT = exposedMethod("MuCtx.insert_element", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_str = _jffiBuffer.getAddress(1)
val _raw_index = _jffiBuffer.getAddress(2)
val _raw_newval = _jffiBuffer.getAddress(3)
val ctx = getMuCtx(_raw_ctx)
val str = getMuValueNotNull(_raw_str).asInstanceOf[MuSeqValue]
val index = getMuValueNotNull(_raw_index).asInstanceOf[MuIntValue]
val newval = getMuValueNotNull(_raw_newval).asInstanceOf[MuValue]
val _RV = ctx.insertElement(str, index, newval)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEW_FIXED = exposedMethod("MuCtx.new_fixed", JType.POINTER, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_mu_type = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val mu_type = _raw_mu_type
val _RV = ctx.newFixed(mu_type)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEW_HYBRID = exposedMethod("MuCtx.new_hybrid", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_mu_type = _jffiBuffer.getInt(1)
val _raw_length = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val mu_type = _raw_mu_type
val length = getMuValueNotNull(_raw_length).asInstanceOf[MuIntValue]
val _RV = ctx.newHybrid(mu_type, length)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__REFCAST = exposedMethod("MuCtx.refcast", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val _raw_new_type = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuGenRefValue]
val new_type = _raw_new_type
val _RV = ctx.refcast(opnd, new_type)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__GET_IREF = exposedMethod("MuCtx.get_iref", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuRefValue]
val _RV = ctx.getIRef(opnd)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__GET_FIELD_IREF = exposedMethod("MuCtx.get_field_iref", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.SINT)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val _raw_field = _jffiBuffer.getInt(2)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIRefValue]
val field = _raw_field
val _RV = ctx.getFieldIRef(opnd, field)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__GET_ELEM_IREF = exposedMethod("MuCtx.get_elem_iref", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val _raw_index = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIRefValue]
val index = getMuValueNotNull(_raw_index).asInstanceOf[MuIntValue]
val _RV = ctx.getElemIRef(opnd, index)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__SHIFT_IREF = exposedMethod("MuCtx.shift_iref", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val _raw_offset = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIRefValue]
val offset = getMuValueNotNull(_raw_offset).asInstanceOf[MuIntValue]
val _RV = ctx.shiftIRef(opnd, offset)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__GET_VAR_PART_IREF = exposedMethod("MuCtx.get_var_part_iref", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_opnd = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuIRefValue]
val _RV = ctx.getVarPartIRef(opnd)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__LOAD = exposedMethod("MuCtx.load", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ord = _jffiBuffer.getInt(1)
val _raw_loc = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val ord = toMemoryOrder(_raw_ord)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuIRefValue]
val _RV = ctx.load(ord, loc)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__STORE = exposedMethod("MuCtx.store", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ord = _jffiBuffer.getInt(1)
val _raw_loc = _jffiBuffer.getAddress(2)
val _raw_newval = _jffiBuffer.getAddress(3)
val ctx = getMuCtx(_raw_ctx)
val ord = toMemoryOrder(_raw_ord)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuIRefValue]
val newval = getMuValueNotNull(_raw_newval).asInstanceOf[MuValue]
val _RV = ctx.store(ord, loc, newval)
}
val MUCTX__CMPXCHG = exposedMethod("MuCtx.cmpxchg", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ord_succ = _jffiBuffer.getInt(1)
val _raw_ord_fail = _jffiBuffer.getInt(2)
val _raw_weak = _jffiBuffer.getInt(3)
val _raw_loc = _jffiBuffer.getAddress(4)
val _raw_expected = _jffiBuffer.getAddress(5)
val _raw_desired = _jffiBuffer.getAddress(6)
val _raw_is_succ = _jffiBuffer.getAddress(7)
val ctx = getMuCtx(_raw_ctx)
val ord_succ = toMemoryOrder(_raw_ord_succ)
val ord_fail = toMemoryOrder(_raw_ord_fail)
val weak = intToBoolean(_raw_weak)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuIRefValue]
val expected = getMuValueNotNull(_raw_expected).asInstanceOf[MuValue]
val desired = getMuValueNotNull(_raw_desired).asInstanceOf[MuValue]
val is_succ = _raw_is_succ
val _RV = ctx.cmpxchg(ord_succ, ord_fail, weak, loc, expected, desired, is_succ)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__ATOMICRMW = exposedMethod("MuCtx.atomicrmw", JType.POINTER, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ord = _jffiBuffer.getInt(1)
val _raw_op = _jffiBuffer.getInt(2)
val _raw_loc = _jffiBuffer.getAddress(3)
val _raw_opnd = _jffiBuffer.getAddress(4)
val ctx = getMuCtx(_raw_ctx)
val ord = toMemoryOrder(_raw_ord)
val op = toAtomicRMWOptr(_raw_op)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuIRefValue]
val opnd = getMuValueNotNull(_raw_opnd).asInstanceOf[MuValue]
val _RV = ctx.atomicrmw(ord, op, loc, opnd)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__FENCE = exposedMethod("MuCtx.fence", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ord = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val ord = toMemoryOrder(_raw_ord)
val _RV = ctx.fence(ord)
}
val MUCTX__NEW_STACK = exposedMethod("MuCtx.new_stack", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_func = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val func = getMuValueNotNull(_raw_func).asInstanceOf[MuFuncRefValue]
val _RV = ctx.newStack(func)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEW_THREAD_NOR = exposedMethod("MuCtx.new_thread_nor", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_stack = _jffiBuffer.getAddress(1)
val _raw_threadlocal = _jffiBuffer.getAddress(2)
val _raw_vals = _jffiBuffer.getAddress(3)
val _raw_nvals = _jffiBuffer.getAddress(4)
val ctx = getMuCtx(_raw_ctx)
val stack = getMuValueNotNull(_raw_stack).asInstanceOf[MuStackRefValue]
val threadlocal = getMuValueNullable(_raw_threadlocal).asInstanceOf[Option[MuRefValue]]
val vals = readMuValueArray(_raw_vals, _raw_nvals)
val _RV = ctx.newThreadNor(stack, threadlocal, vals)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEW_THREAD_EXC = exposedMethod("MuCtx.new_thread_exc", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_stack = _jffiBuffer.getAddress(1)
val _raw_threadlocal = _jffiBuffer.getAddress(2)
val _raw_exc = _jffiBuffer.getAddress(3)
val ctx = getMuCtx(_raw_ctx)
val stack = getMuValueNotNull(_raw_stack).asInstanceOf[MuStackRefValue]
val threadlocal = getMuValueNullable(_raw_threadlocal).asInstanceOf[Option[MuRefValue]]
val exc = getMuValueNotNull(_raw_exc).asInstanceOf[MuRefValue]
val _RV = ctx.newThreadExc(stack, threadlocal, exc)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__KILL_STACK = exposedMethod("MuCtx.kill_stack", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_stack = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val stack = getMuValueNotNull(_raw_stack).asInstanceOf[MuStackRefValue]
val _RV = ctx.killStack(stack)
}
val MUCTX__SET_THREADLOCAL = exposedMethod("MuCtx.set_threadlocal", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_thread = _jffiBuffer.getAddress(1)
val _raw_threadlocal = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val thread = getMuValueNotNull(_raw_thread).asInstanceOf[MuThreadRefValue]
val threadlocal = getMuValueNotNull(_raw_threadlocal).asInstanceOf[MuRefValue]
val _RV = ctx.setThreadlocal(thread, threadlocal)
}
val MUCTX__GET_THREADLOCAL = exposedMethod("MuCtx.get_threadlocal", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_thread = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val thread = getMuValueNotNull(_raw_thread).asInstanceOf[MuThreadRefValue]
val _RV = ctx.getThreadlocal(thread)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEW_CURSOR = exposedMethod("MuCtx.new_cursor", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_stack = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val stack = getMuValueNotNull(_raw_stack).asInstanceOf[MuStackRefValue]
val _RV = ctx.newCursor(stack)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__NEXT_FRAME = exposedMethod("MuCtx.next_frame", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.nextFrame(cursor)
}
val MUCTX__COPY_CURSOR = exposedMethod("MuCtx.copy_cursor", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.copyCursor(cursor)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__CLOSE_CURSOR = exposedMethod("MuCtx.close_cursor", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.closeCursor(cursor)
}
val MUCTX__CUR_FUNC = exposedMethod("MuCtx.cur_func", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.curFunc(cursor)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__CUR_FUNC_VER = exposedMethod("MuCtx.cur_func_ver", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.curFuncVer(cursor)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__CUR_INST = exposedMethod("MuCtx.cur_inst", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.curInst(cursor)
_jffiBuffer.setIntReturn(_RV)
}
val MUCTX__DUMP_KEEPALIVES = exposedMethod("MuCtx.dump_keepalives", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val _raw_results = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val results = _raw_results
val _RV = ctx.dumpKeepalives(cursor, results)
}
val MUCTX__POP_FRAMES_TO = exposedMethod("MuCtx.pop_frames_to", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val _RV = ctx.popFramesTo(cursor)
}
val MUCTX__PUSH_FRAME = exposedMethod("MuCtx.push_frame", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_cursor = _jffiBuffer.getAddress(1)
val _raw_func = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val cursor = getMuValueNotNull(_raw_cursor).asInstanceOf[MuFCRefValue]
val func = getMuValueNotNull(_raw_func).asInstanceOf[MuFuncRefValue]
val _RV = ctx.pushFrame(cursor, func)
}
val MUCTX__TR64_IS_FP = exposedMethod("MuCtx.tr64_is_fp", JType.SINT, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64IsFP(value)
val _RV_FAK = booleanToInt(_RV)
_jffiBuffer.setIntReturn(_RV_FAK)
}
val MUCTX__TR64_IS_INT = exposedMethod("MuCtx.tr64_is_int", JType.SINT, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64IsInt(value)
val _RV_FAK = booleanToInt(_RV)
_jffiBuffer.setIntReturn(_RV_FAK)
}
val MUCTX__TR64_IS_REF = exposedMethod("MuCtx.tr64_is_ref", JType.SINT, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64IsRef(value)
val _RV_FAK = booleanToInt(_RV)
_jffiBuffer.setIntReturn(_RV_FAK)
}
val MUCTX__TR64_TO_FP = exposedMethod("MuCtx.tr64_to_fp", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64ToFP(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_TO_INT = exposedMethod("MuCtx.tr64_to_int", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64ToInt(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_TO_REF = exposedMethod("MuCtx.tr64_to_ref", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64ToRef(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_TO_TAG = exposedMethod("MuCtx.tr64_to_tag", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuTagRef64Value]
val _RV = ctx.tr64ToTag(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_FROM_FP = exposedMethod("MuCtx.tr64_from_fp", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuDoubleValue]
val _RV = ctx.tr64FromFP(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_FROM_INT = exposedMethod("MuCtx.tr64_from_int", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_value = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuIntValue]
val _RV = ctx.tr64FromInt(value)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__TR64_FROM_REF = exposedMethod("MuCtx.tr64_from_ref", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_ref = _jffiBuffer.getAddress(1)
val _raw_tag = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val ref = getMuValueNotNull(_raw_ref).asInstanceOf[MuRefValue]
val tag = getMuValueNotNull(_raw_tag).asInstanceOf[MuIntValue]
val _RV = ctx.tr64FromRef(ref, tag)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__ENABLE_WATCHPOINT = exposedMethod("MuCtx.enable_watchpoint", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_wpid = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val wpid = _raw_wpid
val _RV = ctx.enableWatchPoint(wpid)
}
val MUCTX__DISABLE_WATCHPOINT = exposedMethod("MuCtx.disable_watchpoint", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_wpid = _jffiBuffer.getInt(1)
val ctx = getMuCtx(_raw_ctx)
val wpid = _raw_wpid
val _RV = ctx.disableWatchPoint(wpid)
}
val MUCTX__PIN = exposedMethod("MuCtx.pin", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_loc = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuValue]
val _RV = ctx.pin(loc)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__UNPIN = exposedMethod("MuCtx.unpin", JType.VOID, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_loc = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuValue]
val _RV = ctx.unpin(loc)
}
val MUCTX__GET_ADDR = exposedMethod("MuCtx.get_addr", JType.POINTER, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_loc = _jffiBuffer.getAddress(1)
val ctx = getMuCtx(_raw_ctx)
val loc = getMuValueNotNull(_raw_loc).asInstanceOf[MuValue]
val _RV = ctx.getAddr(loc)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__EXPOSE = exposedMethod("MuCtx.expose", JType.POINTER, Array(JType.POINTER, JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_func = _jffiBuffer.getAddress(1)
val _raw_call_conv = _jffiBuffer.getInt(2)
val _raw_cookie = _jffiBuffer.getAddress(3)
val ctx = getMuCtx(_raw_ctx)
val func = getMuValueNotNull(_raw_func).asInstanceOf[MuFuncRefValue]
val call_conv = toCallConv(_raw_call_conv)
val cookie = getMuValueNotNull(_raw_cookie).asInstanceOf[MuIntValue]
val _RV = ctx.expose(func, call_conv, cookie)
val _RV_FAK = exposeMuValue(ctx, _RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__UNEXPOSE = exposedMethod("MuCtx.unexpose", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_call_conv = _jffiBuffer.getInt(1)
val _raw_value = _jffiBuffer.getAddress(2)
val ctx = getMuCtx(_raw_ctx)
val call_conv = toCallConv(_raw_call_conv)
val value = getMuValueNotNull(_raw_value).asInstanceOf[MuValue]
val _RV = ctx.unexpose(call_conv, value)
}
val MUCTX__NEW_IR_BUILDER = exposedMethod("MuCtx.new_ir_builder", JType.POINTER, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val ctx = getMuCtx(_raw_ctx)
val _RV = ctx.newIRBuilder()
val _RV_FAK = exposeMuIRBuilder(_RV)
_jffiBuffer.setAddressReturn(_RV_FAK)
}
val MUCTX__MAKE_BOOT_IMAGE = exposedMethod("MuCtx.make_boot_image", JType.VOID, Array(JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_ctx = _jffiBuffer.getAddress(0)
val _raw_whitelist = _jffiBuffer.getAddress(1)
val _raw_whitelist_sz = _jffiBuffer.getAddress(2)
val _raw_primordial_func = _jffiBuffer.getAddress(3)
val _raw_primordial_stack = _jffiBuffer.getAddress(4)
val _raw_primordial_threadlocal = _jffiBuffer.getAddress(5)
val _raw_sym_fields = _jffiBuffer.getAddress(6)
val _raw_sym_strings = _jffiBuffer.getAddress(7)
val _raw_nsyms = _jffiBuffer.getAddress(8)
val _raw_reloc_fields = _jffiBuffer.getAddress(9)
val _raw_reloc_strings = _jffiBuffer.getAddress(10)
val _raw_nrelocs = _jffiBuffer.getAddress(11)
val _raw_output_file = _jffiBuffer.getAddress(12)
val ctx = getMuCtx(_raw_ctx)
val whitelist = readIntArray(_raw_whitelist, _raw_whitelist_sz)
val primordial_func = getMuValueNullable(_raw_primordial_func).asInstanceOf[Option[MuFuncRefValue]]
val primordial_stack = getMuValueNullable(_raw_primordial_stack).asInstanceOf[Option[MuStackRefValue]]
val primordial_threadlocal = getMuValueNullable(_raw_primordial_threadlocal).asInstanceOf[Option[MuRefValue]]
val sym_fields = readMuValueArray(_raw_sym_fields, _raw_nsyms)
val sym_strings = readCStringArray(_raw_sym_strings, _raw_nsyms)
val reloc_fields = readMuValueArray(_raw_reloc_fields, _raw_nrelocs)
val reloc_strings = readCStringArray(_raw_reloc_strings, _raw_nrelocs)
val output_file = readCString(_raw_output_file)
val _RV = ctx.makeBootImage(whitelist, primordial_func, primordial_stack, primordial_threadlocal, sym_fields, sym_strings, reloc_fields, reloc_strings, output_file)
}
val stubsOfMuCtx = new Array[Word](89)
stubsOfMuCtx(0) = MUCTX__ID_OF.address
stubsOfMuCtx(1) = MUCTX__NAME_OF.address
stubsOfMuCtx(2) = MUCTX__CLOSE_CONTEXT.address
stubsOfMuCtx(3) = MUCTX__LOAD_BUNDLE.address
stubsOfMuCtx(4) = MUCTX__LOAD_HAIL.address
stubsOfMuCtx(5) = MUCTX__HANDLE_FROM_SINT8.address
stubsOfMuCtx(6) = MUCTX__HANDLE_FROM_UINT8.address
stubsOfMuCtx(7) = MUCTX__HANDLE_FROM_SINT16.address
stubsOfMuCtx(8) = MUCTX__HANDLE_FROM_UINT16.address
stubsOfMuCtx(9) = MUCTX__HANDLE_FROM_SINT32.address
stubsOfMuCtx(10) = MUCTX__HANDLE_FROM_UINT32.address
stubsOfMuCtx(11) = MUCTX__HANDLE_FROM_SINT64.address
stubsOfMuCtx(12) = MUCTX__HANDLE_FROM_UINT64.address
stubsOfMuCtx(13) = MUCTX__HANDLE_FROM_UINT64S.address
stubsOfMuCtx(14) = MUCTX__HANDLE_FROM_FLOAT.address
stubsOfMuCtx(15) = MUCTX__HANDLE_FROM_DOUBLE.address
stubsOfMuCtx(16) = MUCTX__HANDLE_FROM_PTR.address
stubsOfMuCtx(17) = MUCTX__HANDLE_FROM_FP.address
stubsOfMuCtx(18) = MUCTX__HANDLE_TO_SINT8.address
stubsOfMuCtx(19) = MUCTX__HANDLE_TO_UINT8.address
stubsOfMuCtx(20) = MUCTX__HANDLE_TO_SINT16.address
stubsOfMuCtx(21) = MUCTX__HANDLE_TO_UINT16.address
stubsOfMuCtx(22) = MUCTX__HANDLE_TO_SINT32.address
stubsOfMuCtx(23) = MUCTX__HANDLE_TO_UINT32.address
stubsOfMuCtx(24) = MUCTX__HANDLE_TO_SINT64.address
stubsOfMuCtx(25) = MUCTX__HANDLE_TO_UINT64.address
stubsOfMuCtx(26) = MUCTX__HANDLE_TO_FLOAT.address
stubsOfMuCtx(27) = MUCTX__HANDLE_TO_DOUBLE.address
stubsOfMuCtx(28) = MUCTX__HANDLE_TO_PTR.address
stubsOfMuCtx(29) = MUCTX__HANDLE_TO_FP.address
stubsOfMuCtx(30) = MUCTX__HANDLE_FROM_CONST.address
stubsOfMuCtx(31) = MUCTX__HANDLE_FROM_GLOBAL.address
stubsOfMuCtx(32) = MUCTX__HANDLE_FROM_FUNC.address
stubsOfMuCtx(33) = MUCTX__HANDLE_FROM_EXPOSE.address
stubsOfMuCtx(34) = MUCTX__DELETE_VALUE.address
stubsOfMuCtx(35) = MUCTX__REF_EQ.address
stubsOfMuCtx(36) = MUCTX__REF_ULT.address
stubsOfMuCtx(37) = MUCTX__EXTRACT_VALUE.address
stubsOfMuCtx(38) = MUCTX__INSERT_VALUE.address
stubsOfMuCtx(39) = MUCTX__EXTRACT_ELEMENT.address
stubsOfMuCtx(40) = MUCTX__INSERT_ELEMENT.address
stubsOfMuCtx(41) = MUCTX__NEW_FIXED.address
stubsOfMuCtx(42) = MUCTX__NEW_HYBRID.address
stubsOfMuCtx(43) = MUCTX__REFCAST.address
stubsOfMuCtx(44) = MUCTX__GET_IREF.address
stubsOfMuCtx(45) = MUCTX__GET_FIELD_IREF.address
stubsOfMuCtx(46) = MUCTX__GET_ELEM_IREF.address
stubsOfMuCtx(47) = MUCTX__SHIFT_IREF.address
stubsOfMuCtx(48) = MUCTX__GET_VAR_PART_IREF.address
stubsOfMuCtx(49) = MUCTX__LOAD.address
stubsOfMuCtx(50) = MUCTX__STORE.address
stubsOfMuCtx(51) = MUCTX__CMPXCHG.address
stubsOfMuCtx(52) = MUCTX__ATOMICRMW.address
stubsOfMuCtx(53) = MUCTX__FENCE.address
stubsOfMuCtx(54) = MUCTX__NEW_STACK.address
stubsOfMuCtx(55) = MUCTX__NEW_THREAD_NOR.address
stubsOfMuCtx(56) = MUCTX__NEW_THREAD_EXC.address
stubsOfMuCtx(57) = MUCTX__KILL_STACK.address
stubsOfMuCtx(58) = MUCTX__SET_THREADLOCAL.address
stubsOfMuCtx(59) = MUCTX__GET_THREADLOCAL.address
stubsOfMuCtx(60) = MUCTX__NEW_CURSOR.address
stubsOfMuCtx(61) = MUCTX__NEXT_FRAME.address
stubsOfMuCtx(62) = MUCTX__COPY_CURSOR.address
stubsOfMuCtx(63) = MUCTX__CLOSE_CURSOR.address
stubsOfMuCtx(64) = MUCTX__CUR_FUNC.address
stubsOfMuCtx(65) = MUCTX__CUR_FUNC_VER.address
stubsOfMuCtx(66) = MUCTX__CUR_INST.address
stubsOfMuCtx(67) = MUCTX__DUMP_KEEPALIVES.address
stubsOfMuCtx(68) = MUCTX__POP_FRAMES_TO.address
stubsOfMuCtx(69) = MUCTX__PUSH_FRAME.address
stubsOfMuCtx(70) = MUCTX__TR64_IS_FP.address
stubsOfMuCtx(71) = MUCTX__TR64_IS_INT.address
stubsOfMuCtx(72) = MUCTX__TR64_IS_REF.address
stubsOfMuCtx(73) = MUCTX__TR64_TO_FP.address
stubsOfMuCtx(74) = MUCTX__TR64_TO_INT.address
stubsOfMuCtx(75) = MUCTX__TR64_TO_REF.address
stubsOfMuCtx(76) = MUCTX__TR64_TO_TAG.address
stubsOfMuCtx(77) = MUCTX__TR64_FROM_FP.address
stubsOfMuCtx(78) = MUCTX__TR64_FROM_INT.address
stubsOfMuCtx(79) = MUCTX__TR64_FROM_REF.address
stubsOfMuCtx(80) = MUCTX__ENABLE_WATCHPOINT.address
stubsOfMuCtx(81) = MUCTX__DISABLE_WATCHPOINT.address
stubsOfMuCtx(82) = MUCTX__PIN.address
stubsOfMuCtx(83) = MUCTX__UNPIN.address
stubsOfMuCtx(84) = MUCTX__GET_ADDR.address
stubsOfMuCtx(85) = MUCTX__EXPOSE.address
stubsOfMuCtx(86) = MUCTX__UNEXPOSE.address
stubsOfMuCtx(87) = MUCTX__NEW_IR_BUILDER.address
stubsOfMuCtx(88) = MUCTX__MAKE_BOOT_IMAGE.address
val MUIRBUILDER__LOAD = exposedMethod("MuIRBuilder.load", JType.VOID, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val b = getMuIRBuilder(_raw_b)
val _RV = b.load()
}
val MUIRBUILDER__ABORT = exposedMethod("MuIRBuilder.abort", JType.VOID, Array(JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val b = getMuIRBuilder(_raw_b)
val _RV = b.abort()
}
val MUIRBUILDER__GEN_SYM = exposedMethod("MuIRBuilder.gen_sym", JType.UINT32, Array(JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_name = _jffiBuffer.getAddress(1)
val b = getMuIRBuilder(_raw_b)
val name = readCStringOptional(_raw_name)
val _RV = b.genSym(name)
_jffiBuffer.setIntReturn(_RV)
}
val MUIRBUILDER__NEW_TYPE_INT = exposedMethod("MuIRBuilder.new_type_int", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.SINT)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_len = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val len = _raw_len
val _RV = b.newTypeInt(id, len)
}
val MUIRBUILDER__NEW_TYPE_FLOAT = exposedMethod("MuIRBuilder.new_type_float", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeFloat(id)
}
val MUIRBUILDER__NEW_TYPE_DOUBLE = exposedMethod("MuIRBuilder.new_type_double", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeDouble(id)
}
val MUIRBUILDER__NEW_TYPE_UPTR = exposedMethod("MuIRBuilder.new_type_uptr", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newTypeUPtr(id, ty)
}
val MUIRBUILDER__NEW_TYPE_UFUNCPTR = exposedMethod("MuIRBuilder.new_type_ufuncptr", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_sig = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val sig = _raw_sig
val _RV = b.newTypeUFuncPtr(id, sig)
}
val MUIRBUILDER__NEW_TYPE_STRUCT = exposedMethod("MuIRBuilder.new_type_struct", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_fieldtys = _jffiBuffer.getAddress(2)
val _raw_nfieldtys = _jffiBuffer.getAddress(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val fieldtys = readMuIDArray(_raw_fieldtys, _raw_nfieldtys)
val _RV = b.newTypeStruct(id, fieldtys)
}
val MUIRBUILDER__NEW_TYPE_HYBRID = exposedMethod("MuIRBuilder.new_type_hybrid", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_fixedtys = _jffiBuffer.getAddress(2)
val _raw_nfixedtys = _jffiBuffer.getAddress(3)
val _raw_varty = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val fixedtys = readMuIDArray(_raw_fixedtys, _raw_nfixedtys)
val varty = _raw_varty
val _RV = b.newTypeHybrid(id, fixedtys, varty)
}
val MUIRBUILDER__NEW_TYPE_ARRAY = exposedMethod("MuIRBuilder.new_type_array", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT64)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_elemty = _jffiBuffer.getInt(2)
val _raw_len = _jffiBuffer.getLong(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val elemty = _raw_elemty
val len = _raw_len
val _RV = b.newTypeArray(id, elemty, len)
}
val MUIRBUILDER__NEW_TYPE_VECTOR = exposedMethod("MuIRBuilder.new_type_vector", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT64)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_elemty = _jffiBuffer.getInt(2)
val _raw_len = _jffiBuffer.getLong(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val elemty = _raw_elemty
val len = _raw_len
val _RV = b.newTypeVector(id, elemty, len)
}
val MUIRBUILDER__NEW_TYPE_VOID = exposedMethod("MuIRBuilder.new_type_void", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeVoid(id)
}
val MUIRBUILDER__NEW_TYPE_REF = exposedMethod("MuIRBuilder.new_type_ref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newTypeRef(id, ty)
}
val MUIRBUILDER__NEW_TYPE_IREF = exposedMethod("MuIRBuilder.new_type_iref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newTypeIRef(id, ty)
}
val MUIRBUILDER__NEW_TYPE_WEAKREF = exposedMethod("MuIRBuilder.new_type_weakref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newTypeWeakRef(id, ty)
}
val MUIRBUILDER__NEW_TYPE_FUNCREF = exposedMethod("MuIRBuilder.new_type_funcref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_sig = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val sig = _raw_sig
val _RV = b.newTypeFuncRef(id, sig)
}
val MUIRBUILDER__NEW_TYPE_TAGREF64 = exposedMethod("MuIRBuilder.new_type_tagref64", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeTagRef64(id)
}
val MUIRBUILDER__NEW_TYPE_THREADREF = exposedMethod("MuIRBuilder.new_type_threadref", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeThreadRef(id)
}
val MUIRBUILDER__NEW_TYPE_STACKREF = exposedMethod("MuIRBuilder.new_type_stackref", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeStackRef(id)
}
val MUIRBUILDER__NEW_TYPE_FRAMECURSORREF = exposedMethod("MuIRBuilder.new_type_framecursorref", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeFrameCursorRef(id)
}
val MUIRBUILDER__NEW_TYPE_IRBUILDERREF = exposedMethod("MuIRBuilder.new_type_irbuilderref", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newTypeIRBuilderRef(id)
}
val MUIRBUILDER__NEW_FUNCSIG = exposedMethod("MuIRBuilder.new_funcsig", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_paramtys = _jffiBuffer.getAddress(2)
val _raw_nparamtys = _jffiBuffer.getAddress(3)
val _raw_rettys = _jffiBuffer.getAddress(4)
val _raw_nrettys = _jffiBuffer.getAddress(5)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val paramtys = readMuIDArray(_raw_paramtys, _raw_nparamtys)
val rettys = readMuIDArray(_raw_rettys, _raw_nrettys)
val _RV = b.newFuncSig(id, paramtys, rettys)
}
val MUIRBUILDER__NEW_CONST_INT = exposedMethod("MuIRBuilder.new_const_int", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT64)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_value = _jffiBuffer.getLong(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val value = _raw_value
val _RV = b.newConstInt(id, ty, value)
}
val MUIRBUILDER__NEW_CONST_INT_EX = exposedMethod("MuIRBuilder.new_const_int_ex", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_values = _jffiBuffer.getAddress(3)
val _raw_nvalues = _jffiBuffer.getAddress(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val values = readLongArray(_raw_values, _raw_nvalues)
val _RV = b.newConstIntEx(id, ty, values)
}
val MUIRBUILDER__NEW_CONST_FLOAT = exposedMethod("MuIRBuilder.new_const_float", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.FLOAT)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_value = _jffiBuffer.getFloat(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val value = _raw_value
val _RV = b.newConstFloat(id, ty, value)
}
val MUIRBUILDER__NEW_CONST_DOUBLE = exposedMethod("MuIRBuilder.new_const_double", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.DOUBLE)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_value = _jffiBuffer.getDouble(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val value = _raw_value
val _RV = b.newConstDouble(id, ty, value)
}
val MUIRBUILDER__NEW_CONST_NULL = exposedMethod("MuIRBuilder.new_const_null", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newConstNull(id, ty)
}
val MUIRBUILDER__NEW_CONST_SEQ = exposedMethod("MuIRBuilder.new_const_seq", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_elems = _jffiBuffer.getAddress(3)
val _raw_nelems = _jffiBuffer.getAddress(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val elems = readMuIDArray(_raw_elems, _raw_nelems)
val _RV = b.newConstSeq(id, ty, elems)
}
val MUIRBUILDER__NEW_CONST_EXTERN = exposedMethod("MuIRBuilder.new_const_extern", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val _raw_symbol = _jffiBuffer.getAddress(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val symbol = readCString(_raw_symbol)
val _RV = b.newConstExtern(id, ty, symbol)
}
val MUIRBUILDER__NEW_GLOBAL_CELL = exposedMethod("MuIRBuilder.new_global_cell", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ty = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ty = _raw_ty
val _RV = b.newGlobalCell(id, ty)
}
val MUIRBUILDER__NEW_FUNC = exposedMethod("MuIRBuilder.new_func", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_sig = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val sig = _raw_sig
val _RV = b.newFunc(id, sig)
}
val MUIRBUILDER__NEW_EXP_FUNC = exposedMethod("MuIRBuilder.new_exp_func", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_func = _jffiBuffer.getInt(2)
val _raw_callconv = _jffiBuffer.getInt(3)
val _raw_cookie = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val func = _raw_func
val callconv = toCallConv(_raw_callconv)
val cookie = _raw_cookie
val _RV = b.newExpFunc(id, func, callconv, cookie)
}
val MUIRBUILDER__NEW_FUNC_VER = exposedMethod("MuIRBuilder.new_func_ver", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_func = _jffiBuffer.getInt(2)
val _raw_bbs = _jffiBuffer.getAddress(3)
val _raw_nbbs = _jffiBuffer.getAddress(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val func = _raw_func
val bbs = readMuIDArray(_raw_bbs, _raw_nbbs)
val _RV = b.newFuncVer(id, func, bbs)
}
val MUIRBUILDER__NEW_BB = exposedMethod("MuIRBuilder.new_bb", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_nor_param_ids = _jffiBuffer.getAddress(2)
val _raw_nor_param_types = _jffiBuffer.getAddress(3)
val _raw_n_nor_params = _jffiBuffer.getAddress(4)
val _raw_exc_param_id = _jffiBuffer.getInt(5)
val _raw_insts = _jffiBuffer.getAddress(6)
val _raw_ninsts = _jffiBuffer.getAddress(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val nor_param_ids = readIntArray(_raw_nor_param_ids, _raw_n_nor_params)
val nor_param_types = readMuIDArray(_raw_nor_param_types, _raw_n_nor_params)
val exc_param_id = readMuIDOptional(_raw_exc_param_id)
val insts = readMuIDArray(_raw_insts, _raw_ninsts)
val _RV = b.newBB(id, nor_param_ids, nor_param_types, exc_param_id, insts)
}
val MUIRBUILDER__NEW_DEST_CLAUSE = exposedMethod("MuIRBuilder.new_dest_clause", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_dest = _jffiBuffer.getInt(2)
val _raw_vars = _jffiBuffer.getAddress(3)
val _raw_nvars = _jffiBuffer.getAddress(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val dest = _raw_dest
val vars = readMuIDArray(_raw_vars, _raw_nvars)
val _RV = b.newDestClause(id, dest, vars)
}
val MUIRBUILDER__NEW_EXC_CLAUSE = exposedMethod("MuIRBuilder.new_exc_clause", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_nor = _jffiBuffer.getInt(2)
val _raw_exc = _jffiBuffer.getInt(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val nor = _raw_nor
val exc = _raw_exc
val _RV = b.newExcClause(id, nor, exc)
}
val MUIRBUILDER__NEW_KEEPALIVE_CLAUSE = exposedMethod("MuIRBuilder.new_keepalive_clause", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_vars = _jffiBuffer.getAddress(2)
val _raw_nvars = _jffiBuffer.getAddress(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val vars = readMuIDArray(_raw_vars, _raw_nvars)
val _RV = b.newKeepaliveClause(id, vars)
}
val MUIRBUILDER__NEW_CSC_RET_WITH = exposedMethod("MuIRBuilder.new_csc_ret_with", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_rettys = _jffiBuffer.getAddress(2)
val _raw_nrettys = _jffiBuffer.getAddress(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val rettys = readMuIDArray(_raw_rettys, _raw_nrettys)
val _RV = b.newCscRetWith(id, rettys)
}
val MUIRBUILDER__NEW_CSC_KILL_OLD = exposedMethod("MuIRBuilder.new_csc_kill_old", JType.VOID, Array(JType.POINTER, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val _RV = b.newCscKillOld(id)
}
val MUIRBUILDER__NEW_NSC_PASS_VALUES = exposedMethod("MuIRBuilder.new_nsc_pass_values", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_tys = _jffiBuffer.getAddress(2)
val _raw_vars = _jffiBuffer.getAddress(3)
val _raw_ntysvars = _jffiBuffer.getAddress(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val tys = readMuIDArray(_raw_tys, _raw_ntysvars)
val vars = readMuIDArray(_raw_vars, _raw_ntysvars)
val _RV = b.newNscPassValues(id, tys, vars)
}
val MUIRBUILDER__NEW_NSC_THROW_EXC = exposedMethod("MuIRBuilder.new_nsc_throw_exc", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_exc = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val exc = _raw_exc
val _RV = b.newNscThrowExc(id, exc)
}
val MUIRBUILDER__NEW_BINOP = exposedMethod("MuIRBuilder.new_binop", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_optr = _jffiBuffer.getInt(3)
val _raw_ty = _jffiBuffer.getInt(4)
val _raw_opnd1 = _jffiBuffer.getInt(5)
val _raw_opnd2 = _jffiBuffer.getInt(6)
val _raw_exc_clause = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val optr = toBinOptr(_raw_optr)
val ty = _raw_ty
val opnd1 = _raw_opnd1
val opnd2 = _raw_opnd2
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newBinOp(id, result_id, optr, ty, opnd1, opnd2, exc_clause)
}
val MUIRBUILDER__NEW_BINOP_WITH_STATUS = exposedMethod("MuIRBuilder.new_binop_with_status", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_status_result_ids = _jffiBuffer.getAddress(3)
val _raw_n_status_result_ids = _jffiBuffer.getAddress(4)
val _raw_optr = _jffiBuffer.getInt(5)
val _raw_status_flags = _jffiBuffer.getInt(6)
val _raw_ty = _jffiBuffer.getInt(7)
val _raw_opnd1 = _jffiBuffer.getInt(8)
val _raw_opnd2 = _jffiBuffer.getInt(9)
val _raw_exc_clause = _jffiBuffer.getInt(10)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val status_result_ids = readIntArray(_raw_status_result_ids, _raw_n_status_result_ids)
val optr = toBinOptr(_raw_optr)
val status_flags = toBinOpStatus(_raw_status_flags)
val ty = _raw_ty
val opnd1 = _raw_opnd1
val opnd2 = _raw_opnd2
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newBinOpWithStatus(id, result_id, status_result_ids, optr, status_flags, ty, opnd1, opnd2, exc_clause)
}
val MUIRBUILDER__NEW_CMP = exposedMethod("MuIRBuilder.new_cmp", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_optr = _jffiBuffer.getInt(3)
val _raw_ty = _jffiBuffer.getInt(4)
val _raw_opnd1 = _jffiBuffer.getInt(5)
val _raw_opnd2 = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val optr = toCmpOptr(_raw_optr)
val ty = _raw_ty
val opnd1 = _raw_opnd1
val opnd2 = _raw_opnd2
val _RV = b.newCmp(id, result_id, optr, ty, opnd1, opnd2)
}
val MUIRBUILDER__NEW_CONV = exposedMethod("MuIRBuilder.new_conv", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_optr = _jffiBuffer.getInt(3)
val _raw_from_ty = _jffiBuffer.getInt(4)
val _raw_to_ty = _jffiBuffer.getInt(5)
val _raw_opnd = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val optr = toConvOptr(_raw_optr)
val from_ty = _raw_from_ty
val to_ty = _raw_to_ty
val opnd = _raw_opnd
val _RV = b.newConv(id, result_id, optr, from_ty, to_ty, opnd)
}
val MUIRBUILDER__NEW_SELECT = exposedMethod("MuIRBuilder.new_select", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_cond_ty = _jffiBuffer.getInt(3)
val _raw_opnd_ty = _jffiBuffer.getInt(4)
val _raw_cond = _jffiBuffer.getInt(5)
val _raw_if_true = _jffiBuffer.getInt(6)
val _raw_if_false = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val cond_ty = _raw_cond_ty
val opnd_ty = _raw_opnd_ty
val cond = _raw_cond
val if_true = _raw_if_true
val if_false = _raw_if_false
val _RV = b.newSelect(id, result_id, cond_ty, opnd_ty, cond, if_true, if_false)
}
val MUIRBUILDER__NEW_BRANCH = exposedMethod("MuIRBuilder.new_branch", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_dest = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val dest = _raw_dest
val _RV = b.newBranch(id, dest)
}
val MUIRBUILDER__NEW_BRANCH2 = exposedMethod("MuIRBuilder.new_branch2", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_cond = _jffiBuffer.getInt(2)
val _raw_if_true = _jffiBuffer.getInt(3)
val _raw_if_false = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val cond = _raw_cond
val if_true = _raw_if_true
val if_false = _raw_if_false
val _RV = b.newBranch2(id, cond, if_true, if_false)
}
val MUIRBUILDER__NEW_SWITCH = exposedMethod("MuIRBuilder.new_switch", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_opnd_ty = _jffiBuffer.getInt(2)
val _raw_opnd = _jffiBuffer.getInt(3)
val _raw_default_dest = _jffiBuffer.getInt(4)
val _raw_cases = _jffiBuffer.getAddress(5)
val _raw_dests = _jffiBuffer.getAddress(6)
val _raw_ncasesdests = _jffiBuffer.getAddress(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val opnd_ty = _raw_opnd_ty
val opnd = _raw_opnd
val default_dest = _raw_default_dest
val cases = readMuIDArray(_raw_cases, _raw_ncasesdests)
val dests = readMuIDArray(_raw_dests, _raw_ncasesdests)
val _RV = b.newSwitch(id, opnd_ty, opnd, default_dest, cases, dests)
}
val MUIRBUILDER__NEW_CALL = exposedMethod("MuIRBuilder.new_call", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_ids = _jffiBuffer.getAddress(2)
val _raw_n_result_ids = _jffiBuffer.getAddress(3)
val _raw_sig = _jffiBuffer.getInt(4)
val _raw_callee = _jffiBuffer.getInt(5)
val _raw_args = _jffiBuffer.getAddress(6)
val _raw_nargs = _jffiBuffer.getAddress(7)
val _raw_exc_clause = _jffiBuffer.getInt(8)
val _raw_keepalive_clause = _jffiBuffer.getInt(9)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_ids = readIntArray(_raw_result_ids, _raw_n_result_ids)
val sig = _raw_sig
val callee = _raw_callee
val args = readMuIDArray(_raw_args, _raw_nargs)
val exc_clause = readMuIDOptional(_raw_exc_clause)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newCall(id, result_ids, sig, callee, args, exc_clause, keepalive_clause)
}
val MUIRBUILDER__NEW_TAILCALL = exposedMethod("MuIRBuilder.new_tailcall", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_sig = _jffiBuffer.getInt(2)
val _raw_callee = _jffiBuffer.getInt(3)
val _raw_args = _jffiBuffer.getAddress(4)
val _raw_nargs = _jffiBuffer.getAddress(5)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val sig = _raw_sig
val callee = _raw_callee
val args = readMuIDArray(_raw_args, _raw_nargs)
val _RV = b.newTailCall(id, sig, callee, args)
}
val MUIRBUILDER__NEW_RET = exposedMethod("MuIRBuilder.new_ret", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_rvs = _jffiBuffer.getAddress(2)
val _raw_nrvs = _jffiBuffer.getAddress(3)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val rvs = readMuIDArray(_raw_rvs, _raw_nrvs)
val _RV = b.newRet(id, rvs)
}
val MUIRBUILDER__NEW_THROW = exposedMethod("MuIRBuilder.new_throw", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_exc = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val exc = _raw_exc
val _RV = b.newThrow(id, exc)
}
val MUIRBUILDER__NEW_EXTRACTVALUE = exposedMethod("MuIRBuilder.new_extractvalue", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_strty = _jffiBuffer.getInt(3)
val _raw_index = _jffiBuffer.getInt(4)
val _raw_opnd = _jffiBuffer.getInt(5)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val strty = _raw_strty
val index = _raw_index
val opnd = _raw_opnd
val _RV = b.newExtractValue(id, result_id, strty, index, opnd)
}
val MUIRBUILDER__NEW_INSERTVALUE = exposedMethod("MuIRBuilder.new_insertvalue", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_strty = _jffiBuffer.getInt(3)
val _raw_index = _jffiBuffer.getInt(4)
val _raw_opnd = _jffiBuffer.getInt(5)
val _raw_newval = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val strty = _raw_strty
val index = _raw_index
val opnd = _raw_opnd
val newval = _raw_newval
val _RV = b.newInsertValue(id, result_id, strty, index, opnd, newval)
}
val MUIRBUILDER__NEW_EXTRACTELEMENT = exposedMethod("MuIRBuilder.new_extractelement", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_seqty = _jffiBuffer.getInt(3)
val _raw_indty = _jffiBuffer.getInt(4)
val _raw_opnd = _jffiBuffer.getInt(5)
val _raw_index = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val seqty = _raw_seqty
val indty = _raw_indty
val opnd = _raw_opnd
val index = _raw_index
val _RV = b.newExtractElement(id, result_id, seqty, indty, opnd, index)
}
val MUIRBUILDER__NEW_INSERTELEMENT = exposedMethod("MuIRBuilder.new_insertelement", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_seqty = _jffiBuffer.getInt(3)
val _raw_indty = _jffiBuffer.getInt(4)
val _raw_opnd = _jffiBuffer.getInt(5)
val _raw_index = _jffiBuffer.getInt(6)
val _raw_newval = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val seqty = _raw_seqty
val indty = _raw_indty
val opnd = _raw_opnd
val index = _raw_index
val newval = _raw_newval
val _RV = b.newInsertElement(id, result_id, seqty, indty, opnd, index, newval)
}
val MUIRBUILDER__NEW_SHUFFLEVECTOR = exposedMethod("MuIRBuilder.new_shufflevector", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_vecty = _jffiBuffer.getInt(3)
val _raw_maskty = _jffiBuffer.getInt(4)
val _raw_vec1 = _jffiBuffer.getInt(5)
val _raw_vec2 = _jffiBuffer.getInt(6)
val _raw_mask = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val vecty = _raw_vecty
val maskty = _raw_maskty
val vec1 = _raw_vec1
val vec2 = _raw_vec2
val mask = _raw_mask
val _RV = b.newShuffleVector(id, result_id, vecty, maskty, vec1, vec2, mask)
}
val MUIRBUILDER__NEW_NEW = exposedMethod("MuIRBuilder.new_new", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_allocty = _jffiBuffer.getInt(3)
val _raw_exc_clause = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val allocty = _raw_allocty
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newNew(id, result_id, allocty, exc_clause)
}
val MUIRBUILDER__NEW_NEWHYBRID = exposedMethod("MuIRBuilder.new_newhybrid", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_allocty = _jffiBuffer.getInt(3)
val _raw_lenty = _jffiBuffer.getInt(4)
val _raw_length = _jffiBuffer.getInt(5)
val _raw_exc_clause = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val allocty = _raw_allocty
val lenty = _raw_lenty
val length = _raw_length
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newNewHybrid(id, result_id, allocty, lenty, length, exc_clause)
}
val MUIRBUILDER__NEW_ALLOCA = exposedMethod("MuIRBuilder.new_alloca", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_allocty = _jffiBuffer.getInt(3)
val _raw_exc_clause = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val allocty = _raw_allocty
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newAlloca(id, result_id, allocty, exc_clause)
}
val MUIRBUILDER__NEW_ALLOCAHYBRID = exposedMethod("MuIRBuilder.new_allocahybrid", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_allocty = _jffiBuffer.getInt(3)
val _raw_lenty = _jffiBuffer.getInt(4)
val _raw_length = _jffiBuffer.getInt(5)
val _raw_exc_clause = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val allocty = _raw_allocty
val lenty = _raw_lenty
val length = _raw_length
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newAllocaHybrid(id, result_id, allocty, lenty, length, exc_clause)
}
val MUIRBUILDER__NEW_GETIREF = exposedMethod("MuIRBuilder.new_getiref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_refty = _jffiBuffer.getInt(3)
val _raw_opnd = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val refty = _raw_refty
val opnd = _raw_opnd
val _RV = b.newGetIRef(id, result_id, refty, opnd)
}
val MUIRBUILDER__NEW_GETFIELDIREF = exposedMethod("MuIRBuilder.new_getfieldiref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.SINT, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_refty = _jffiBuffer.getInt(4)
val _raw_index = _jffiBuffer.getInt(5)
val _raw_opnd = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val refty = _raw_refty
val index = _raw_index
val opnd = _raw_opnd
val _RV = b.newGetFieldIRef(id, result_id, is_ptr, refty, index, opnd)
}
val MUIRBUILDER__NEW_GETELEMIREF = exposedMethod("MuIRBuilder.new_getelemiref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_refty = _jffiBuffer.getInt(4)
val _raw_indty = _jffiBuffer.getInt(5)
val _raw_opnd = _jffiBuffer.getInt(6)
val _raw_index = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val refty = _raw_refty
val indty = _raw_indty
val opnd = _raw_opnd
val index = _raw_index
val _RV = b.newGetElemIRef(id, result_id, is_ptr, refty, indty, opnd, index)
}
val MUIRBUILDER__NEW_SHIFTIREF = exposedMethod("MuIRBuilder.new_shiftiref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_refty = _jffiBuffer.getInt(4)
val _raw_offty = _jffiBuffer.getInt(5)
val _raw_opnd = _jffiBuffer.getInt(6)
val _raw_offset = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val refty = _raw_refty
val offty = _raw_offty
val opnd = _raw_opnd
val offset = _raw_offset
val _RV = b.newShiftIRef(id, result_id, is_ptr, refty, offty, opnd, offset)
}
val MUIRBUILDER__NEW_GETVARPARTIREF = exposedMethod("MuIRBuilder.new_getvarpartiref", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_refty = _jffiBuffer.getInt(4)
val _raw_opnd = _jffiBuffer.getInt(5)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val refty = _raw_refty
val opnd = _raw_opnd
val _RV = b.newGetVarPartIRef(id, result_id, is_ptr, refty, opnd)
}
val MUIRBUILDER__NEW_LOAD = exposedMethod("MuIRBuilder.new_load", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_ord = _jffiBuffer.getInt(4)
val _raw_refty = _jffiBuffer.getInt(5)
val _raw_loc = _jffiBuffer.getInt(6)
val _raw_exc_clause = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val ord = toMemoryOrder(_raw_ord)
val refty = _raw_refty
val loc = _raw_loc
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newLoad(id, result_id, is_ptr, ord, refty, loc, exc_clause)
}
val MUIRBUILDER__NEW_STORE = exposedMethod("MuIRBuilder.new_store", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_is_ptr = _jffiBuffer.getInt(2)
val _raw_ord = _jffiBuffer.getInt(3)
val _raw_refty = _jffiBuffer.getInt(4)
val _raw_loc = _jffiBuffer.getInt(5)
val _raw_newval = _jffiBuffer.getInt(6)
val _raw_exc_clause = _jffiBuffer.getInt(7)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val is_ptr = intToBoolean(_raw_is_ptr)
val ord = toMemoryOrder(_raw_ord)
val refty = _raw_refty
val loc = _raw_loc
val newval = _raw_newval
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newStore(id, is_ptr, ord, refty, loc, newval, exc_clause)
}
val MUIRBUILDER__NEW_CMPXCHG = exposedMethod("MuIRBuilder.new_cmpxchg", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.SINT, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_value_result_id = _jffiBuffer.getInt(2)
val _raw_succ_result_id = _jffiBuffer.getInt(3)
val _raw_is_ptr = _jffiBuffer.getInt(4)
val _raw_is_weak = _jffiBuffer.getInt(5)
val _raw_ord_succ = _jffiBuffer.getInt(6)
val _raw_ord_fail = _jffiBuffer.getInt(7)
val _raw_refty = _jffiBuffer.getInt(8)
val _raw_loc = _jffiBuffer.getInt(9)
val _raw_expected = _jffiBuffer.getInt(10)
val _raw_desired = _jffiBuffer.getInt(11)
val _raw_exc_clause = _jffiBuffer.getInt(12)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val value_result_id = _raw_value_result_id
val succ_result_id = _raw_succ_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val is_weak = intToBoolean(_raw_is_weak)
val ord_succ = toMemoryOrder(_raw_ord_succ)
val ord_fail = toMemoryOrder(_raw_ord_fail)
val refty = _raw_refty
val loc = _raw_loc
val expected = _raw_expected
val desired = _raw_desired
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newCmpXchg(id, value_result_id, succ_result_id, is_ptr, is_weak, ord_succ, ord_fail, refty, loc, expected, desired, exc_clause)
}
val MUIRBUILDER__NEW_ATOMICRMW = exposedMethod("MuIRBuilder.new_atomicrmw", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.SINT, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_is_ptr = _jffiBuffer.getInt(3)
val _raw_ord = _jffiBuffer.getInt(4)
val _raw_optr = _jffiBuffer.getInt(5)
val _raw_ref_ty = _jffiBuffer.getInt(6)
val _raw_loc = _jffiBuffer.getInt(7)
val _raw_opnd = _jffiBuffer.getInt(8)
val _raw_exc_clause = _jffiBuffer.getInt(9)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val is_ptr = intToBoolean(_raw_is_ptr)
val ord = toMemoryOrder(_raw_ord)
val optr = toAtomicRMWOptr(_raw_optr)
val ref_ty = _raw_ref_ty
val loc = _raw_loc
val opnd = _raw_opnd
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newAtomicRMW(id, result_id, is_ptr, ord, optr, ref_ty, loc, opnd, exc_clause)
}
val MUIRBUILDER__NEW_FENCE = exposedMethod("MuIRBuilder.new_fence", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_ord = _jffiBuffer.getInt(2)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val ord = toMemoryOrder(_raw_ord)
val _RV = b.newFence(id, ord)
}
val MUIRBUILDER__NEW_TRAP = exposedMethod("MuIRBuilder.new_trap", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_ids = _jffiBuffer.getAddress(2)
val _raw_rettys = _jffiBuffer.getAddress(3)
val _raw_nretvals = _jffiBuffer.getAddress(4)
val _raw_exc_clause = _jffiBuffer.getInt(5)
val _raw_keepalive_clause = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_ids = readIntArray(_raw_result_ids, _raw_nretvals)
val rettys = readMuIDArray(_raw_rettys, _raw_nretvals)
val exc_clause = readMuIDOptional(_raw_exc_clause)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newTrap(id, result_ids, rettys, exc_clause, keepalive_clause)
}
val MUIRBUILDER__NEW_WATCHPOINT = exposedMethod("MuIRBuilder.new_watchpoint", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_wpid = _jffiBuffer.getInt(2)
val _raw_result_ids = _jffiBuffer.getAddress(3)
val _raw_rettys = _jffiBuffer.getAddress(4)
val _raw_nretvals = _jffiBuffer.getAddress(5)
val _raw_dis = _jffiBuffer.getInt(6)
val _raw_ena = _jffiBuffer.getInt(7)
val _raw_exc = _jffiBuffer.getInt(8)
val _raw_keepalive_clause = _jffiBuffer.getInt(9)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val wpid = _raw_wpid
val result_ids = readIntArray(_raw_result_ids, _raw_nretvals)
val rettys = readMuIDArray(_raw_rettys, _raw_nretvals)
val dis = _raw_dis
val ena = _raw_ena
val exc = readMuIDOptional(_raw_exc)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newWatchPoint(id, wpid, result_ids, rettys, dis, ena, exc, keepalive_clause)
}
val MUIRBUILDER__NEW_WPBRANCH = exposedMethod("MuIRBuilder.new_wpbranch", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_wpid = _jffiBuffer.getInt(2)
val _raw_dis = _jffiBuffer.getInt(3)
val _raw_ena = _jffiBuffer.getInt(4)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val wpid = _raw_wpid
val dis = _raw_dis
val ena = _raw_ena
val _RV = b.newWPBranch(id, wpid, dis, ena)
}
val MUIRBUILDER__NEW_CCALL = exposedMethod("MuIRBuilder.new_ccall", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_ids = _jffiBuffer.getAddress(2)
val _raw_n_result_ids = _jffiBuffer.getAddress(3)
val _raw_callconv = _jffiBuffer.getInt(4)
val _raw_callee_ty = _jffiBuffer.getInt(5)
val _raw_sig = _jffiBuffer.getInt(6)
val _raw_callee = _jffiBuffer.getInt(7)
val _raw_args = _jffiBuffer.getAddress(8)
val _raw_nargs = _jffiBuffer.getAddress(9)
val _raw_exc_clause = _jffiBuffer.getInt(10)
val _raw_keepalive_clause = _jffiBuffer.getInt(11)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_ids = readIntArray(_raw_result_ids, _raw_n_result_ids)
val callconv = toCallConv(_raw_callconv)
val callee_ty = _raw_callee_ty
val sig = _raw_sig
val callee = _raw_callee
val args = readMuIDArray(_raw_args, _raw_nargs)
val exc_clause = readMuIDOptional(_raw_exc_clause)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newCCall(id, result_ids, callconv, callee_ty, sig, callee, args, exc_clause, keepalive_clause)
}
val MUIRBUILDER__NEW_NEWTHREAD = exposedMethod("MuIRBuilder.new_newthread", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_id = _jffiBuffer.getInt(2)
val _raw_stack = _jffiBuffer.getInt(3)
val _raw_threadlocal = _jffiBuffer.getInt(4)
val _raw_new_stack_clause = _jffiBuffer.getInt(5)
val _raw_exc_clause = _jffiBuffer.getInt(6)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_id = _raw_result_id
val stack = _raw_stack
val threadlocal = readMuIDOptional(_raw_threadlocal)
val new_stack_clause = _raw_new_stack_clause
val exc_clause = readMuIDOptional(_raw_exc_clause)
val _RV = b.newNewThread(id, result_id, stack, threadlocal, new_stack_clause, exc_clause)
}
val MUIRBUILDER__NEW_SWAPSTACK = exposedMethod("MuIRBuilder.new_swapstack", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_ids = _jffiBuffer.getAddress(2)
val _raw_n_result_ids = _jffiBuffer.getAddress(3)
val _raw_swappee = _jffiBuffer.getInt(4)
val _raw_cur_stack_clause = _jffiBuffer.getInt(5)
val _raw_new_stack_clause = _jffiBuffer.getInt(6)
val _raw_exc_clause = _jffiBuffer.getInt(7)
val _raw_keepalive_clause = _jffiBuffer.getInt(8)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_ids = readIntArray(_raw_result_ids, _raw_n_result_ids)
val swappee = _raw_swappee
val cur_stack_clause = _raw_cur_stack_clause
val new_stack_clause = _raw_new_stack_clause
val exc_clause = readMuIDOptional(_raw_exc_clause)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newSwapStack(id, result_ids, swappee, cur_stack_clause, new_stack_clause, exc_clause, keepalive_clause)
}
val MUIRBUILDER__NEW_COMMINST = exposedMethod("MuIRBuilder.new_comminst", JType.VOID, Array(JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.UINT32, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.POINTER, JType.UINT32, JType.UINT32)) { _jffiBuffer =>
val _raw_b = _jffiBuffer.getAddress(0)
val _raw_id = _jffiBuffer.getInt(1)
val _raw_result_ids = _jffiBuffer.getAddress(2)
val _raw_n_result_ids = _jffiBuffer.getAddress(3)
val _raw_opcode = _jffiBuffer.getInt(4)
val _raw_flags = _jffiBuffer.getAddress(5)
val _raw_nflags = _jffiBuffer.getAddress(6)
val _raw_tys = _jffiBuffer.getAddress(7)
val _raw_ntys = _jffiBuffer.getAddress(8)
val _raw_sigs = _jffiBuffer.getAddress(9)
val _raw_nsigs = _jffiBuffer.getAddress(10)
val _raw_args = _jffiBuffer.getAddress(11)
val _raw_nargs = _jffiBuffer.getAddress(12)
val _raw_exc_clause = _jffiBuffer.getInt(13)
val _raw_keepalive_clause = _jffiBuffer.getInt(14)
val b = getMuIRBuilder(_raw_b)
val id = _raw_id
val result_ids = readIntArray(_raw_result_ids, _raw_n_result_ids)
val opcode = toCommInst(_raw_opcode)
val flags = readFlagArray(_raw_flags, _raw_nflags)
val tys = readMuIDArray(_raw_tys, _raw_ntys)
val sigs = readMuIDArray(_raw_sigs, _raw_nsigs)
val args = readMuIDArray(_raw_args, _raw_nargs)
val exc_clause = readMuIDOptional(_raw_exc_clause)
val keepalive_clause = readMuIDOptional(_raw_keepalive_clause)
val _RV = b.newCommInst(id, result_ids, opcode, flags, tys, sigs, args, exc_clause, keepalive_clause)
}
val stubsOfMuIRBuilder = new Array[Word](80)
stubsOfMuIRBuilder(0) = MUIRBUILDER__LOAD.address
stubsOfMuIRBuilder(1) = MUIRBUILDER__ABORT.address
stubsOfMuIRBuilder(2) = MUIRBUILDER__GEN_SYM.address
stubsOfMuIRBuilder(3) = MUIRBUILDER__NEW_TYPE_INT.address
stubsOfMuIRBuilder(4) = MUIRBUILDER__NEW_TYPE_FLOAT.address
stubsOfMuIRBuilder(5) = MUIRBUILDER__NEW_TYPE_DOUBLE.address
stubsOfMuIRBuilder(6) = MUIRBUILDER__NEW_TYPE_UPTR.address
stubsOfMuIRBuilder(7) = MUIRBUILDER__NEW_TYPE_UFUNCPTR.address
stubsOfMuIRBuilder(8) = MUIRBUILDER__NEW_TYPE_STRUCT.address
stubsOfMuIRBuilder(9) = MUIRBUILDER__NEW_TYPE_HYBRID.address
stubsOfMuIRBuilder(10) = MUIRBUILDER__NEW_TYPE_ARRAY.address
stubsOfMuIRBuilder(11) = MUIRBUILDER__NEW_TYPE_VECTOR.address
stubsOfMuIRBuilder(12) = MUIRBUILDER__NEW_TYPE_VOID.address
stubsOfMuIRBuilder(13) = MUIRBUILDER__NEW_TYPE_REF.address
stubsOfMuIRBuilder(14) = MUIRBUILDER__NEW_TYPE_IREF.address
stubsOfMuIRBuilder(15) = MUIRBUILDER__NEW_TYPE_WEAKREF.address
stubsOfMuIRBuilder(16) = MUIRBUILDER__NEW_TYPE_FUNCREF.address
stubsOfMuIRBuilder(17) = MUIRBUILDER__NEW_TYPE_TAGREF64.address
stubsOfMuIRBuilder(18) = MUIRBUILDER__NEW_TYPE_THREADREF.address
stubsOfMuIRBuilder(19) = MUIRBUILDER__NEW_TYPE_STACKREF.address
stubsOfMuIRBuilder(20) = MUIRBUILDER__NEW_TYPE_FRAMECURSORREF.address
stubsOfMuIRBuilder(21) = MUIRBUILDER__NEW_TYPE_IRBUILDERREF.address
stubsOfMuIRBuilder(22) = MUIRBUILDER__NEW_FUNCSIG.address
stubsOfMuIRBuilder(23) = MUIRBUILDER__NEW_CONST_INT.address
stubsOfMuIRBuilder(24) = MUIRBUILDER__NEW_CONST_INT_EX.address
stubsOfMuIRBuilder(25) = MUIRBUILDER__NEW_CONST_FLOAT.address
stubsOfMuIRBuilder(26) = MUIRBUILDER__NEW_CONST_DOUBLE.address
stubsOfMuIRBuilder(27) = MUIRBUILDER__NEW_CONST_NULL.address
stubsOfMuIRBuilder(28) = MUIRBUILDER__NEW_CONST_SEQ.address
stubsOfMuIRBuilder(29) = MUIRBUILDER__NEW_CONST_EXTERN.address
stubsOfMuIRBuilder(30) = MUIRBUILDER__NEW_GLOBAL_CELL.address
stubsOfMuIRBuilder(31) = MUIRBUILDER__NEW_FUNC.address
stubsOfMuIRBuilder(32) = MUIRBUILDER__NEW_EXP_FUNC.address
stubsOfMuIRBuilder(33) = MUIRBUILDER__NEW_FUNC_VER.address
stubsOfMuIRBuilder(34) = MUIRBUILDER__NEW_BB.address
stubsOfMuIRBuilder(35) = MUIRBUILDER__NEW_DEST_CLAUSE.address
stubsOfMuIRBuilder(36) = MUIRBUILDER__NEW_EXC_CLAUSE.address
stubsOfMuIRBuilder(37) = MUIRBUILDER__NEW_KEEPALIVE_CLAUSE.address
stubsOfMuIRBuilder(38) = MUIRBUILDER__NEW_CSC_RET_WITH.address
stubsOfMuIRBuilder(39) = MUIRBUILDER__NEW_CSC_KILL_OLD.address
stubsOfMuIRBuilder(40) = MUIRBUILDER__NEW_NSC_PASS_VALUES.address
stubsOfMuIRBuilder(41) = MUIRBUILDER__NEW_NSC_THROW_EXC.address
stubsOfMuIRBuilder(42) = MUIRBUILDER__NEW_BINOP.address
stubsOfMuIRBuilder(43) = MUIRBUILDER__NEW_BINOP_WITH_STATUS.address
stubsOfMuIRBuilder(44) = MUIRBUILDER__NEW_CMP.address
stubsOfMuIRBuilder(45) = MUIRBUILDER__NEW_CONV.address
stubsOfMuIRBuilder(46) = MUIRBUILDER__NEW_SELECT.address
stubsOfMuIRBuilder(47) = MUIRBUILDER__NEW_BRANCH.address
stubsOfMuIRBuilder(48) = MUIRBUILDER__NEW_BRANCH2.address
stubsOfMuIRBuilder(49) = MUIRBUILDER__NEW_SWITCH.address
stubsOfMuIRBuilder(50) = MUIRBUILDER__NEW_CALL.address
stubsOfMuIRBuilder(51) = MUIRBUILDER__NEW_TAILCALL.address
stubsOfMuIRBuilder(52) = MUIRBUILDER__NEW_RET.address
stubsOfMuIRBuilder(53) = MUIRBUILDER__NEW_THROW.address
stubsOfMuIRBuilder(54) = MUIRBUILDER__NEW_EXTRACTVALUE.address
stubsOfMuIRBuilder(55) = MUIRBUILDER__NEW_INSERTVALUE.address
stubsOfMuIRBuilder(56) = MUIRBUILDER__NEW_EXTRACTELEMENT.address
stubsOfMuIRBuilder(57) = MUIRBUILDER__NEW_INSERTELEMENT.address
stubsOfMuIRBuilder(58) = MUIRBUILDER__NEW_SHUFFLEVECTOR.address
stubsOfMuIRBuilder(59) = MUIRBUILDER__NEW_NEW.address
stubsOfMuIRBuilder(60) = MUIRBUILDER__NEW_NEWHYBRID.address
stubsOfMuIRBuilder(61) = MUIRBUILDER__NEW_ALLOCA.address
stubsOfMuIRBuilder(62) = MUIRBUILDER__NEW_ALLOCAHYBRID.address
stubsOfMuIRBuilder(63) = MUIRBUILDER__NEW_GETIREF.address
stubsOfMuIRBuilder(64) = MUIRBUILDER__NEW_GETFIELDIREF.address
stubsOfMuIRBuilder(65) = MUIRBUILDER__NEW_GETELEMIREF.address
stubsOfMuIRBuilder(66) = MUIRBUILDER__NEW_SHIFTIREF.address
stubsOfMuIRBuilder(67) = MUIRBUILDER__NEW_GETVARPARTIREF.address
stubsOfMuIRBuilder(68) = MUIRBUILDER__NEW_LOAD.address
stubsOfMuIRBuilder(69) = MUIRBUILDER__NEW_STORE.address
stubsOfMuIRBuilder(70) = MUIRBUILDER__NEW_CMPXCHG.address
stubsOfMuIRBuilder(71) = MUIRBUILDER__NEW_ATOMICRMW.address
stubsOfMuIRBuilder(72) = MUIRBUILDER__NEW_FENCE.address
stubsOfMuIRBuilder(73) = MUIRBUILDER__NEW_TRAP.address
stubsOfMuIRBuilder(74) = MUIRBUILDER__NEW_WATCHPOINT.address
stubsOfMuIRBuilder(75) = MUIRBUILDER__NEW_WPBRANCH.address
stubsOfMuIRBuilder(76) = MUIRBUILDER__NEW_CCALL.address
stubsOfMuIRBuilder(77) = MUIRBUILDER__NEW_NEWTHREAD.address
stubsOfMuIRBuilder(78) = MUIRBUILDER__NEW_SWAPSTACK.address
stubsOfMuIRBuilder(79) = MUIRBUILDER__NEW_COMMINST.address
val MU_THREAD_EXIT = 0x00
val MU_REBIND_PASS_VALUES = 0x01
val MU_REBIND_THROW_EXC = 0x02
val MU_BOS_N = 0x01
val MU_BOS_Z = 0x02
val MU_BOS_C = 0x04
val MU_BOS_V = 0x08
val MU_BINOP_ADD = 0x01
val MU_BINOP_SUB = 0x02
val MU_BINOP_MUL = 0x03
val MU_BINOP_SDIV = 0x04
val MU_BINOP_SREM = 0x05
val MU_BINOP_UDIV = 0x06
val MU_BINOP_UREM = 0x07
val MU_BINOP_SHL = 0x08
val MU_BINOP_LSHR = 0x09
val MU_BINOP_ASHR = 0x0A
val MU_BINOP_AND = 0x0B
val MU_BINOP_OR = 0x0C
val MU_BINOP_XOR = 0x0D
val MU_BINOP_FADD = 0xB0
val MU_BINOP_FSUB = 0xB1
val MU_BINOP_FMUL = 0xB2
val MU_BINOP_FDIV = 0xB3
val MU_BINOP_FREM = 0xB4
val MU_CMP_EQ = 0x20
val MU_CMP_NE = 0x21
val MU_CMP_SGE = 0x22
val MU_CMP_SGT = 0x23
val MU_CMP_SLE = 0x24
val MU_CMP_SLT = 0x25
val MU_CMP_UGE = 0x26
val MU_CMP_UGT = 0x27
val MU_CMP_ULE = 0x28
val MU_CMP_ULT = 0x29
val MU_CMP_FFALSE = 0xC0
val MU_CMP_FTRUE = 0xC1
val MU_CMP_FUNO = 0xC2
val MU_CMP_FUEQ = 0xC3
val MU_CMP_FUNE = 0xC4
val MU_CMP_FUGT = 0xC5
val MU_CMP_FUGE = 0xC6
val MU_CMP_FULT = 0xC7
val MU_CMP_FULE = 0xC8
val MU_CMP_FORD = 0xC9
val MU_CMP_FOEQ = 0xCA
val MU_CMP_FONE = 0xCB
val MU_CMP_FOGT = 0xCC
val MU_CMP_FOGE = 0xCD
val MU_CMP_FOLT = 0xCE
val MU_CMP_FOLE = 0xCF
val MU_CONV_TRUNC = 0x30
val MU_CONV_ZEXT = 0x31
val MU_CONV_SEXT = 0x32
val MU_CONV_FPTRUNC = 0x33
val MU_CONV_FPEXT = 0x34
val MU_CONV_FPTOUI = 0x35
val MU_CONV_FPTOSI = 0x36
val MU_CONV_UITOFP = 0x37
val MU_CONV_SITOFP = 0x38
val MU_CONV_BITCAST = 0x39
val MU_CONV_REFCAST = 0x3A
val MU_CONV_PTRCAST = 0x3B
val MU_ORD_NOT_ATOMIC = 0x00
val MU_ORD_RELAXED = 0x01
val MU_ORD_CONSUME = 0x02
val MU_ORD_ACQUIRE = 0x03
val MU_ORD_RELEASE = 0x04
val MU_ORD_ACQ_REL = 0x05
val MU_ORD_SEQ_CST = 0x06
val MU_ARMW_XCHG = 0x00
val MU_ARMW_ADD = 0x01
val MU_ARMW_SUB = 0x02
val MU_ARMW_AND = 0x03
val MU_ARMW_NAND = 0x04
val MU_ARMW_OR = 0x05
val MU_ARMW_XOR = 0x06
val MU_ARMW_MAX = 0x07
val MU_ARMW_MIN = 0x08
val MU_ARMW_UMAX = 0x09
val MU_ARMW_UMIN = 0x0A
val MU_CC_DEFAULT = 0x00
val MU_CI_UVM_NEW_STACK = 0x201
val MU_CI_UVM_KILL_STACK = 0x202
val MU_CI_UVM_THREAD_EXIT = 0x203
val MU_CI_UVM_CURRENT_STACK = 0x204
val MU_CI_UVM_SET_THREADLOCAL = 0x205
val MU_CI_UVM_GET_THREADLOCAL = 0x206
val MU_CI_UVM_TR64_IS_FP = 0x211
val MU_CI_UVM_TR64_IS_INT = 0x212
val MU_CI_UVM_TR64_IS_REF = 0x213
val MU_CI_UVM_TR64_FROM_FP = 0x214
val MU_CI_UVM_TR64_FROM_INT = 0x215
val MU_CI_UVM_TR64_FROM_REF = 0x216
val MU_CI_UVM_TR64_TO_FP = 0x217
val MU_CI_UVM_TR64_TO_INT = 0x218
val MU_CI_UVM_TR64_TO_REF = 0x219
val MU_CI_UVM_TR64_TO_TAG = 0x21a
val MU_CI_UVM_FUTEX_WAIT = 0x220
val MU_CI_UVM_FUTEX_WAIT_TIMEOUT = 0x221
val MU_CI_UVM_FUTEX_WAKE = 0x222
val MU_CI_UVM_FUTEX_CMP_REQUEUE = 0x223
val MU_CI_UVM_KILL_DEPENDENCY = 0x230
val MU_CI_UVM_NATIVE_PIN = 0x240
val MU_CI_UVM_NATIVE_UNPIN = 0x241
val MU_CI_UVM_NATIVE_GET_ADDR = 0x242
val MU_CI_UVM_NATIVE_EXPOSE = 0x243
val MU_CI_UVM_NATIVE_UNEXPOSE = 0x244
val MU_CI_UVM_NATIVE_GET_COOKIE = 0x245
val MU_CI_UVM_META_ID_OF = 0x250
val MU_CI_UVM_META_NAME_OF = 0x251
val MU_CI_UVM_META_LOAD_BUNDLE = 0x252
val MU_CI_UVM_META_LOAD_HAIL = 0x253
val MU_CI_UVM_META_NEW_CURSOR = 0x254
val MU_CI_UVM_META_NEXT_FRAME = 0x255
val MU_CI_UVM_META_COPY_CURSOR = 0x256
val MU_CI_UVM_META_CLOSE_CURSOR = 0x257
val MU_CI_UVM_META_CUR_FUNC = 0x258
val MU_CI_UVM_META_CUR_FUNC_VER = 0x259
val MU_CI_UVM_META_CUR_INST = 0x25a
val MU_CI_UVM_META_DUMP_KEEPALIVES = 0x25b
val MU_CI_UVM_META_POP_FRAMES_TO = 0x25c
val MU_CI_UVM_META_PUSH_FRAME = 0x25d
val MU_CI_UVM_META_ENABLE_WATCHPOINT = 0x25e
val MU_CI_UVM_META_DISABLE_WATCHPOINT = 0x25f
val MU_CI_UVM_META_SET_TRAP_HANDLER = 0x260
val MU_CI_UVM_META_CONSTANT_BY_ID = 0x268
val MU_CI_UVM_META_GLOBAL_BY_ID = 0x269
val MU_CI_UVM_META_FUNC_BY_ID = 0x26a
val MU_CI_UVM_META_EXPFUNC_BY_ID = 0x26b
val MU_CI_UVM_IRBUILDER_NEW_IR_BUILDER = 0x270
val MU_CI_UVM_IRBUILDER_LOAD = 0x300
val MU_CI_UVM_IRBUILDER_ABORT = 0x301
val MU_CI_UVM_IRBUILDER_GEN_SYM = 0x302
val MU_CI_UVM_IRBUILDER_NEW_TYPE_INT = 0x303
val MU_CI_UVM_IRBUILDER_NEW_TYPE_FLOAT = 0x304
val MU_CI_UVM_IRBUILDER_NEW_TYPE_DOUBLE = 0x305
val MU_CI_UVM_IRBUILDER_NEW_TYPE_UPTR = 0x306
val MU_CI_UVM_IRBUILDER_NEW_TYPE_UFUNCPTR = 0x307
val MU_CI_UVM_IRBUILDER_NEW_TYPE_STRUCT = 0x308
val MU_CI_UVM_IRBUILDER_NEW_TYPE_HYBRID = 0x309
val MU_CI_UVM_IRBUILDER_NEW_TYPE_ARRAY = 0x30a
val MU_CI_UVM_IRBUILDER_NEW_TYPE_VECTOR = 0x30b
val MU_CI_UVM_IRBUILDER_NEW_TYPE_VOID = 0x30c
val MU_CI_UVM_IRBUILDER_NEW_TYPE_REF = 0x30d
val MU_CI_UVM_IRBUILDER_NEW_TYPE_IREF = 0x30e
val MU_CI_UVM_IRBUILDER_NEW_TYPE_WEAKREF = 0x30f
val MU_CI_UVM_IRBUILDER_NEW_TYPE_FUNCREF = 0x310
val MU_CI_UVM_IRBUILDER_NEW_TYPE_TAGREF64 = 0x311
val MU_CI_UVM_IRBUILDER_NEW_TYPE_THREADREF = 0x312
val MU_CI_UVM_IRBUILDER_NEW_TYPE_STACKREF = 0x313
val MU_CI_UVM_IRBUILDER_NEW_TYPE_FRAMECURSORREF = 0x314
val MU_CI_UVM_IRBUILDER_NEW_TYPE_IRBUILDERREF = 0x315
val MU_CI_UVM_IRBUILDER_NEW_FUNCSIG = 0x316
val MU_CI_UVM_IRBUILDER_NEW_CONST_INT = 0x317
val MU_CI_UVM_IRBUILDER_NEW_CONST_INT_EX = 0x318
val MU_CI_UVM_IRBUILDER_NEW_CONST_FLOAT = 0x319
val MU_CI_UVM_IRBUILDER_NEW_CONST_DOUBLE = 0x31a
val MU_CI_UVM_IRBUILDER_NEW_CONST_NULL = 0x31b
val MU_CI_UVM_IRBUILDER_NEW_CONST_SEQ = 0x31c
val MU_CI_UVM_IRBUILDER_NEW_CONST_EXTERN = 0x31d
val MU_CI_UVM_IRBUILDER_NEW_GLOBAL_CELL = 0x31e
val MU_CI_UVM_IRBUILDER_NEW_FUNC = 0x31f
val MU_CI_UVM_IRBUILDER_NEW_EXP_FUNC = 0x320
val MU_CI_UVM_IRBUILDER_NEW_FUNC_VER = 0x321
val MU_CI_UVM_IRBUILDER_NEW_BB = 0x322
val MU_CI_UVM_IRBUILDER_NEW_DEST_CLAUSE = 0x323
val MU_CI_UVM_IRBUILDER_NEW_EXC_CLAUSE = 0x324
val MU_CI_UVM_IRBUILDER_NEW_KEEPALIVE_CLAUSE = 0x325
val MU_CI_UVM_IRBUILDER_NEW_CSC_RET_WITH = 0x326
val MU_CI_UVM_IRBUILDER_NEW_CSC_KILL_OLD = 0x327
val MU_CI_UVM_IRBUILDER_NEW_NSC_PASS_VALUES = 0x328
val MU_CI_UVM_IRBUILDER_NEW_NSC_THROW_EXC = 0x329
val MU_CI_UVM_IRBUILDER_NEW_BINOP = 0x32a
val MU_CI_UVM_IRBUILDER_NEW_BINOP_WITH_STATUS = 0x32b
val MU_CI_UVM_IRBUILDER_NEW_CMP = 0x32c
val MU_CI_UVM_IRBUILDER_NEW_CONV = 0x32d
val MU_CI_UVM_IRBUILDER_NEW_SELECT = 0x32e
val MU_CI_UVM_IRBUILDER_NEW_BRANCH = 0x32f
val MU_CI_UVM_IRBUILDER_NEW_BRANCH2 = 0x330
val MU_CI_UVM_IRBUILDER_NEW_SWITCH = 0x331
val MU_CI_UVM_IRBUILDER_NEW_CALL = 0x332
val MU_CI_UVM_IRBUILDER_NEW_TAILCALL = 0x333
val MU_CI_UVM_IRBUILDER_NEW_RET = 0x334
val MU_CI_UVM_IRBUILDER_NEW_THROW = 0x335
val MU_CI_UVM_IRBUILDER_NEW_EXTRACTVALUE = 0x336
val MU_CI_UVM_IRBUILDER_NEW_INSERTVALUE = 0x337
val MU_CI_UVM_IRBUILDER_NEW_EXTRACTELEMENT = 0x338
val MU_CI_UVM_IRBUILDER_NEW_INSERTELEMENT = 0x339
val MU_CI_UVM_IRBUILDER_NEW_SHUFFLEVECTOR = 0x33a
val MU_CI_UVM_IRBUILDER_NEW_NEW = 0x33b
val MU_CI_UVM_IRBUILDER_NEW_NEWHYBRID = 0x33c
val MU_CI_UVM_IRBUILDER_NEW_ALLOCA = 0x33d
val MU_CI_UVM_IRBUILDER_NEW_ALLOCAHYBRID = 0x33e
val MU_CI_UVM_IRBUILDER_NEW_GETIREF = 0x33f
val MU_CI_UVM_IRBUILDER_NEW_GETFIELDIREF = 0x340
val MU_CI_UVM_IRBUILDER_NEW_GETELEMIREF = 0x341
val MU_CI_UVM_IRBUILDER_NEW_SHIFTIREF = 0x342
val MU_CI_UVM_IRBUILDER_NEW_GETVARPARTIREF = 0x343
val MU_CI_UVM_IRBUILDER_NEW_LOAD = 0x344
val MU_CI_UVM_IRBUILDER_NEW_STORE = 0x345
val MU_CI_UVM_IRBUILDER_NEW_CMPXCHG = 0x346
val MU_CI_UVM_IRBUILDER_NEW_ATOMICRMW = 0x347
val MU_CI_UVM_IRBUILDER_NEW_FENCE = 0x348
val MU_CI_UVM_IRBUILDER_NEW_TRAP = 0x349
val MU_CI_UVM_IRBUILDER_NEW_WATCHPOINT = 0x34a
val MU_CI_UVM_IRBUILDER_NEW_WPBRANCH = 0x34b
val MU_CI_UVM_IRBUILDER_NEW_CCALL = 0x34c
val MU_CI_UVM_IRBUILDER_NEW_NEWTHREAD = 0x34d
val MU_CI_UVM_IRBUILDER_NEW_SWAPSTACK = 0x34e
val MU_CI_UVM_IRBUILDER_NEW_COMMINST = 0x34f
val MU_CI_UVM_EXT_PRINT_STATS = 0xc001
val MU_CI_UVM_EXT_CLEAR_STATS = 0xc002
def toBinOptr(cval: MuBinOptr): BinOptr.Value = cval match {
  case 0x01 => BinOptr.ADD
  case 0x02 => BinOptr.SUB
  case 0x03 => BinOptr.MUL
  case 0x04 => BinOptr.SDIV
  case 0x05 => BinOptr.SREM
  case 0x06 => BinOptr.UDIV
  case 0x07 => BinOptr.UREM
  case 0x08 => BinOptr.SHL
  case 0x09 => BinOptr.LSHR
  case 0x0A => BinOptr.ASHR
  case 0x0B => BinOptr.AND
  case 0x0C => BinOptr.OR
  case 0x0D => BinOptr.XOR
  case 0xB0 => BinOptr.FADD
  case 0xB1 => BinOptr.FSUB
  case 0xB2 => BinOptr.FMUL
  case 0xB3 => BinOptr.FDIV
  case 0xB4 => BinOptr.FREM
}
def toCmpOptr(cval: MuCmpOptr): CmpOptr.Value = cval match {
  case 0x20 => CmpOptr.EQ
  case 0x21 => CmpOptr.NE
  case 0x22 => CmpOptr.SGE
  case 0x23 => CmpOptr.SGT
  case 0x24 => CmpOptr.SLE
  case 0x25 => CmpOptr.SLT
  case 0x26 => CmpOptr.UGE
  case 0x27 => CmpOptr.UGT
  case 0x28 => CmpOptr.ULE
  case 0x29 => CmpOptr.ULT
  case 0xC0 => CmpOptr.FFALSE
  case 0xC1 => CmpOptr.FTRUE
  case 0xC2 => CmpOptr.FUNO
  case 0xC3 => CmpOptr.FUEQ
  case 0xC4 => CmpOptr.FUNE
  case 0xC5 => CmpOptr.FUGT
  case 0xC6 => CmpOptr.FUGE
  case 0xC7 => CmpOptr.FULT
  case 0xC8 => CmpOptr.FULE
  case 0xC9 => CmpOptr.FORD
  case 0xCA => CmpOptr.FOEQ
  case 0xCB => CmpOptr.FONE
  case 0xCC => CmpOptr.FOGT
  case 0xCD => CmpOptr.FOGE
  case 0xCE => CmpOptr.FOLT
  case 0xCF => CmpOptr.FOLE
}
def toConvOptr(cval: MuConvOptr): ConvOptr.Value = cval match {
  case 0x30 => ConvOptr.TRUNC
  case 0x31 => ConvOptr.ZEXT
  case 0x32 => ConvOptr.SEXT
  case 0x33 => ConvOptr.FPTRUNC
  case 0x34 => ConvOptr.FPEXT
  case 0x35 => ConvOptr.FPTOUI
  case 0x36 => ConvOptr.FPTOSI
  case 0x37 => ConvOptr.UITOFP
  case 0x38 => ConvOptr.SITOFP
  case 0x39 => ConvOptr.BITCAST
  case 0x3A => ConvOptr.REFCAST
  case 0x3B => ConvOptr.PTRCAST
}
def toMemoryOrder(cval: MuMemOrd): MemoryOrder.Value = cval match {
  case 0x00 => MemoryOrder.NOT_ATOMIC
  case 0x01 => MemoryOrder.RELAXED
  case 0x02 => MemoryOrder.CONSUME
  case 0x03 => MemoryOrder.ACQUIRE
  case 0x04 => MemoryOrder.RELEASE
  case 0x05 => MemoryOrder.ACQ_REL
  case 0x06 => MemoryOrder.SEQ_CST
}
def toAtomicRMWOptr(cval: MuAtomicRMWOptr): AtomicRMWOptr.Value = cval match {
  case 0x00 => AtomicRMWOptr.XCHG
  case 0x01 => AtomicRMWOptr.ADD
  case 0x02 => AtomicRMWOptr.SUB
  case 0x03 => AtomicRMWOptr.AND
  case 0x04 => AtomicRMWOptr.NAND
  case 0x05 => AtomicRMWOptr.OR
  case 0x06 => AtomicRMWOptr.XOR
  case 0x07 => AtomicRMWOptr.MAX
  case 0x08 => AtomicRMWOptr.MIN
  case 0x09 => AtomicRMWOptr.UMAX
  case 0x0A => AtomicRMWOptr.UMIN
}
  /// GEN:END:STUBS
}
