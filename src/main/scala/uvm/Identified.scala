package uvm

trait Identified {
  import Identified._
  def id: MuID
  def name: Option[MuName]

  def repr: String = "[%d:%s]".format(id, name.getOrElse("_"))
  
  // Identified objects should use reference equality rather than structural equality. (case classes use the latter)
  override def hashCode(): Int = if (id != 0) id else System.identityHashCode(this)
  override def equals(that: Any): Boolean = that match {
    case v: AnyRef => this eq v
    case _         => false
  }
  override def toString = "%s%s".format(this.getClass.getSimpleName, this.repr)
}

object Identified {
  val INVALID_ID = 0
  val FIRST_PRIVATE_ID = 32768
  val FIRST_CLIENT_USABLE_ID = 65536
  
  type MuID = Int
  type MuName = String
}

trait IdentifiedSettable extends Identified {
  var id: MuID = 0
  var name: Option[MuName] = None
}

object RichIdentifiedSettable {
  implicit class RichIdentifiedSettable[T <: IdentifiedSettable](val is: T) extends AnyVal {
    def :=(p: MuID): T = {
      is.id = p
      is
    }
    def :=(p: (MuID, MuName)): T = {
      is.id = p._1
      is.name = Some(p._2)
      is
    }
  }
}
