package junks

import java.io.Reader
import java.io.StringReader
import java.io.StringWriter

import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import uvm.GlobalBundle
import uvm.TrantientBundle
import uvm.ir.textinput.UIRTextReader
import uvm.ir.textoutput.BundleSerializer
import uvm.testutil.StopWatch
import uvm.utils.IDFactory

object DumpLoadTimer extends App {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))

  def parseReader(reader: Reader, globalBundle: GlobalBundle, fac: Option[IDFactory] = None): TrantientBundle = {
    val idf = fac.getOrElse(IDFactory.newClientIDFactory())
    val r = new UIRTextReader(idf, recordSourceInfo = false)
    val ir = r.read(reader, globalBundle)
    ir
  }
  def parseFile(fileName: String, globalBundle: GlobalBundle, fac: Option[IDFactory] = None): TrantientBundle = {
    parseReader(new java.io.FileReader(fileName), globalBundle, fac)
  }

  // Replace this with a big UIR bundle
  val fileName = "/Users/wks/junk/RPySOM-no-jit.uir"

  def main(): Unit = {

    val w = new StopWatch()

    w.start()
    val gb = new GlobalBundle()
    logger.info("Parsing (fresh) %s ...".format(fileName))
    val tb = parseFile(fileName, gb)
    gb.merge(tb)

    val s1 = w.split()

    logger.info("Dumping loaded bundle...")
    val sw = new StringWriter()
    val bs = new BundleSerializer(gb, gb.allTopLevels().toSet)
    bs.writeUIR(sw)

    val bTxt = sw.toString()

    logger.debug("Bundle dumped.")
    //logger.debug("Bundle:\n" + bTxt)

    val s2 = w.split()

    val gb2 = new GlobalBundle()
    val sr = new StringReader(bTxt)

    logger.info("Load bundle again ...".format(fileName))
    val tb2 = parseReader(sr, gb2)
    gb2.merge(tb2)

    val s3 = w.stop()

    val total = w.total()

    logger.info("load1: %d, dump: %d, load2: %d, total: %d".format(s1, s2, s3, total))

  }

  main()

}