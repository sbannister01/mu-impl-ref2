package uvm.ir.textinput

import org.scalatest.FlatSpec
import org.scalatest.Matchers
import uvm.GlobalBundle
import uvm.TrantientBundle
import uvm.utils.IDFactory
import uvm.UvmTestBase

class NicerErrorMessage extends UvmTestBase with TestingBundlesValidators with IRParsing {

  behavior of "UIRTextReader"

  it should "give nice error messages" in {
    try {
      val gb = new GlobalBundle()
      val b = parseFile("tests/uvm-parsing-test/bundle-with-error.uir", gb)
    } catch {
      case e: TextIRParsingException => // expected
        travisFriendlyExceptionPrint(e)
    }
  }

}