package uvm.ir.textoutput

import uvm.GlobalBundle
import uvm.UvmTestBase
import uvm.TrantientBundle
import uvm.utils.IDFactory
import uvm.ir.textinput.UIRTextReader
import uvm.ir.textinput.TestingBundlesValidators
import com.typesafe.scalalogging.Logger
import org.slf4j.LoggerFactory
import java.io.StringWriter
import java.io.StringReader
import java.io.Reader
import uvm.ir.textinput.IRParsing

object BundleSerializerSmokeTest {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class BundleSerializerSmokeTest extends UvmTestBase with TestingBundlesValidators with IRParsing {
  import BundleSerializerSmokeTest._
  
  behavior of "BundleSerializer"

  def parseFresh(fileName: String): GlobalBundle = {
    val gb = new GlobalBundle()
    logger.info("Parsing (fresh) %s ...".format(fileName))
    val tb = parseFile(fileName, gb)
    gb.merge(tb)
    
    logger.info("Dumping loaded bundle...")
    val sw = new StringWriter()
    val bs = new BundleSerializer(gb, gb.allTopLevels().toSet)
    bs.writeUIR(sw)
    
    val bTxt = sw.toString()
    
    logger.debug("Bundle:\n" + bTxt)
    
    val gb2 = new GlobalBundle()
    val sr = new StringReader(bTxt)

    logger.info("Load bundle again ...".format(fileName))
    val tb2 = parseReader(sr, gb2)
    gb2.merge(tb2)

    gb2
  }

  it should "reload simple type definitions" in {
    val b = parseFresh("tests/uvm-parsing-test/types.uir")
    validateTypes(b)
  }
  it should "reload simple constant definitions" in {
    val b = parseFresh("tests/uvm-parsing-test/constants.uir")
    validateConstants(b)
  }
  it should "reload simple function definitions" in {
    val b = parseFresh("tests/uvm-parsing-test/functions.uir")
    validateFunctions(b)
  }
  it should "reload simple instruction definitions" in {
    val b = parseFresh("tests/uvm-parsing-test/instructions.uir")
    validateInstructions(b)
  }
  /*
  it should "handle loading of multiple bundles" in {
    val idf = new IDFactory()
    val gb = new GlobalBundle()
    val b1 = parseFile("tests/uvm-parsing-test/redef-base.uir", gb, Some(idf))
    gb.merge(b1)
    val b2 = parseFile("tests/uvm-parsing-test/redef-overlay.uir", gb, Some(idf))
    validateRedef(gb, b1, b2)

    gb.merge(b2)
    validateRedefAfterMerge(gb, b2)
  }
  */
}