package uvm.refimpl.bootimg

import uvm.ir.textinput.ExtraMatchers
import uvm.ir.textinput.TestingBundlesValidators.MagicalOur

import uvm.refimpl.MicroVM
import uvm.refimpl.UvmBundleTesterBase
import uvm.refimpl.VMConf
import uvm.utils.WithUtils._

class BootImageEchoExample extends UvmBundleTesterBase with ExtraMatchers {
  preloadBundles("tests/uvm-refimpl-test/boot-image-echo.uir")
  preloadHails("tests/uvm-refimpl-test/boot-image-echo.hail")

  val our = new MagicalOur(microVM.globalBundle)

  behavior of "The boot image echo example"
  
  it should "write to the boot image" in {
    val filename = "target/boot-image-echo.muref"
    val everything = Seq("@main", "@charpp").map(idOf)

    tryWithResource(microVM.newContext()) { ctx =>
      val func = ctx.handleFromFunc("@main")
      ctx.makeBootImage(everything, Some(func), None, None, Seq(), Seq(), Seq(), Seq(), filename)
      
    }
  }
}