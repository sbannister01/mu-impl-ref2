package uvm.refimpl

import java.io.FileReader

import org.scalatest._
import org.slf4j.LoggerFactory

import com.typesafe.scalalogging.Logger

import uvm._
import uvm.refimpl._
import uvm.refimpl.HowToResume.PassValues
import uvm.refimpl.TrapHandlerResult.Rebind
import uvm.refimpl.itpr._
import uvm.ssavariables._
import uvm.ssavariables.AtomicRMWOptr._
import uvm.ssavariables.MemoryOrder._
import uvm.types._

object UvmBundleTesterBase {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

abstract class UvmBundleTesterBase extends UvmTestBase {
  def makeMicroVM(): MicroVM = MicroVM()

  val microVM = makeMicroVM()

  implicit def idOf(name: String): Int = microVM.idOf(name)
  implicit def nameOf(id: Int): String = microVM.nameOf(id)

  def preloadBundles(fileNames: String*): Unit = {
    val ctx = microVM.newContext()

    for (fn <- fileNames) {
      val r = new FileReader(fn)
      ctx.loadBundle(r)
      r.close()
    }

    ctx.closeContext()
  }

  def preloadHails(fileNames: String*): Unit = {
    val ctx = microVM.newContext()

    for (fn <- fileNames) {
      val r = new FileReader(fn)
      ctx.loadHail(r)
      r.close()
    }

    ctx.closeContext()
  }

  type TrapHandlerFunction = (MuCtx, MuThreadRefValue, MuStackRefValue, Int) => TrapHandlerResult

  class MockTrapHandler(thf: TrapHandlerFunction) extends TrapHandler {
    def handleTrap(ctx: MuCtx, thread: MuThreadRefValue, stack: MuStackRefValue, watchPointID: Int): TrapHandlerResult = {
      thf(ctx, thread, stack, watchPointID)
    }
  }

  def testFunc(ctx: MuCtx, func: MuFuncRefValue, args: Seq[MuValue], threadLocal: Option[MuRefValue]=None
      )(handler: TrapHandlerFunction): Unit = {
    microVM.setTrapHandler(new MockTrapHandler(handler))
    val hStack = ctx.newStack(func)
    val hThread = ctx.newThread(hStack, threadLocal, HowToResume.PassValues(args))
    microVM.execute()
  }

  import UvmBundleTesterBase._
  
  implicit def magicalMuValue(mv: MuValue): MagicalBox = MagicalBox(mv.vb)

  implicit def richMuCtx(ctx: MuCtx) = RichMuCtx.RichMuCtx(ctx)

  def returnFromTrap(st: MuStackRefValue) = Rebind(st, PassValues(Seq()))
}
