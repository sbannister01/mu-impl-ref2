package uvm.refimpl.itpr

import org.scalatest._
import java.io.FileReader
import uvm._
import uvm.types._
import uvm.ssavariables._
import uvm.refimpl._
import uvm.refimpl.itpr._
import MemoryOrder._
import AtomicRMWOptr._
import uvm.refimpl.Word
import ch.qos.logback.classic.Level._
import uvm.refimpl.UvmBundleTesterBase
import uvm.refimpl.TrapHandlerResult.{ ThreadExit, Rebind }
import uvm.refimpl.HowToResume.{ PassValues, ThrowExc }
import uvm.ir.irbuilder.IRBuilder
import uvm.ir.textinput.ExtraMatchers

class UvmInterpreterIRBuilderTests extends UvmBundleTesterBase with ExtraMatchers {
  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl.itpr" -> DEBUG)

  preloadBundles("tests/uvm-refimpl-test/primitives.uir",
    "tests/uvm-refimpl-test/irbuilder-tests.uir")
  preloadHails("tests/uvm-refimpl-test/irbuilder-tests.hail")

  // Dump bundles to see run-time-loaded bundles
  override def makeMicroVM = MicroVM(new VMConf(sourceInfo=true, dumpBundle=true))

  "The new_ir_builder COMMINST" should "create an IR builder" in {
    val ctx = microVM.newContext()

    val func = ctx.handleFromFunc("@irbuilder_test")

    testFunc(ctx, func, Seq()) { (ctx, th, st, wp) =>
      val trapName = nameOf(ctx.curInst(st, 0))

      trapName match {
        case "@irbuilder_test.v1.entry.trap" => {
          val Seq(irb: MuOpaqueRefValue[_], id1: MuIntValue) = ctx.dumpKeepalives(st, 0)

          irb.vb.obj.get shouldBeA[IRBuilder] thatsIt
          (id1.asInt32 >= 65536) shouldBe true

          returnFromTrap(st)
        }
        case "@irbuilder_test.v1.entry.trap2" => {
          val Seq(mySecondConstByID: MuIntValue, id2: MuIntValue) = ctx.dumpKeepalives(st, 0)
          
          mySecondConstByID.asInt32 shouldBe 50
          
          val id2_i32 = id2.asInt32.toInt
          val myname = microVM.nameOf(id2_i32)
          myname shouldEqual "@irbuilder_test.hello"

          returnFromTrap(st)
        }
        case _ => fail("Should not hit " + trapName)
      }
    }

    ctx.closeContext()
  }
}