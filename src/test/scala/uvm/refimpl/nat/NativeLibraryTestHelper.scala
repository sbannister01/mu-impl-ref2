package uvm.refimpl.nat

import com.kenai.jffi.Library
import uvm.refimpl.MicroVM

object NativeLibraryTestHelper {
  def getLibPath(name: String): String = {
    val relPath = s"tests/c-snippets/${name}.so"
    if (!new java.io.File(relPath).isFile()) {
      throw new RuntimeException(s"Need to compile the ${name}.so library. cd into tests/c-snippets and invoke 'make'.")
    }
    relPath
  }

  def loadTestLibrary(name: String): Library = {
    val relPath = getLibPath(name)
    Library.openLibrary(relPath, Library.NOW)
  }
  
  def loadTestLibraryToMicroVM(microVM: MicroVM, name: String): Unit = {
    val relPath = getLibPath(name)
    microVM.nativeLibraryHolder.loadLibrary(relPath)
  }
}