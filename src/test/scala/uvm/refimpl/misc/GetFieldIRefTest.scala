package uvm.refimpl.misc

import org.scalatest._
import java.io.FileReader
import uvm._
import uvm.types._
import uvm.ssavariables._
import uvm.refimpl._
import uvm.refimpl.itpr._
import MemoryOrder._
import AtomicRMWOptr._
import uvm.refimpl.Word
import ch.qos.logback.classic.Level._
import uvm.refimpl.UvmBundleTesterBase
import org.slf4j.LoggerFactory
import com.typesafe.scalalogging.Logger
import uvm.refimpl.TrapHandlerResult.Rebind
import uvm.refimpl.HowToResume.ThrowExc

object GetFieldIRefTest {
  val logger = Logger(LoggerFactory.getLogger(getClass.getName))
}

class GetFieldIRefTest extends UvmBundleTesterBase {
  import GetFieldIRefTest._

  setLogLevels(
    ROOT_LOGGER_NAME -> INFO,
    "uvm.refimpl.misc" -> DEBUG,
    "uvm.refimpl.itpr" -> DEBUG)

  preloadBundles("tests/uvm-refimpl-test/primitives.uir")

  "GetFieldIRef" should "not work if the argument is a ref" in {
    var exceptionCaught: Boolean = false

    try {
    	preloadBundles("tests/uvm-refimpl-test/getfieldireftest.uir")
    } catch {
      case e: Exception => {
        logger.info("Caught exception. This is expected.", e)
        exceptionCaught = true
      }
    }
    
    if (!exceptionCaught) {
      fail("Exception expected, but not caught")
    }
  }
}