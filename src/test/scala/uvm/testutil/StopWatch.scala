package uvm.testutil

object StopWatch {
  def measureTime(f: => Unit): Long = {
    val t1 = System.nanoTime()
    f
    val t2 = System.nanoTime()

    t2 - t1
  }
}

class StopWatch {
  var startTime: Long = _
  var lastSplit: Long = _
  var stopTime: Long = _

  def start(): Unit = {
    val t = System.nanoTime()
    startTime = t
    lastSplit = t
  }

  def split(): Long = {
    val t = System.nanoTime()
    val result = t - lastSplit
    lastSplit = t

    result
  }

  def stop(): Long = {
    val t = System.nanoTime()
    val result = t - lastSplit
    lastSplit = t
    stopTime = t

    result
  }

  def total(): Long = {
    stopTime - startTime
  }
}